"""user-registration-update2

Revision ID: 9bfeb440e897
Revises: bd2138f5fb1d
Create Date: 2020-10-01 22:14:00.536720

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '9bfeb440e897'
down_revision = 'bd2138f5fb1d'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('user', schema=None) as batch_op:
        batch_op.add_column(sa.Column('RACGP_no', sa.String(length=15), nullable=True))
        batch_op.add_column(sa.Column('aboriginal_origin', sa.Boolean(), nullable=True))
        batch_op.add_column(sa.Column('agent', sa.String(length=63), nullable=True))
        batch_op.add_column(sa.Column('alternative_email_address', sa.String(length=120), nullable=True))
        batch_op.add_column(sa.Column('attending_other_school', sa.Boolean(), nullable=True))
        batch_op.add_column(sa.Column('city_of_birth', sa.String(length=63), nullable=True))
        batch_op.add_column(sa.Column('coach_mentor', sa.String(length=63), nullable=True))
        batch_op.add_column(sa.Column('comments', sa.Text(), nullable=True))
        batch_op.add_column(sa.Column('contact_active', sa.Boolean(), nullable=True))
        batch_op.add_column(sa.Column('contact_categories', sa.String(length=63), nullable=True))
        batch_op.add_column(sa.Column('contact_name', sa.String(length=63), nullable=True))
        batch_op.add_column(sa.Column('contact_number', sa.String(length=15), nullable=True))
        batch_op.add_column(sa.Column('contact_source', sa.String(length=63), nullable=True))
        batch_op.add_column(sa.Column('country_of_birth', sa.String(length=31), nullable=True))
        batch_op.add_column(sa.Column('country_of_citizenship', sa.String(length=31), nullable=True))
        batch_op.add_column(sa.Column('division', sa.String(length=63), nullable=True))
        batch_op.add_column(sa.Column('dob', sa.Date(), nullable=True))
        batch_op.add_column(sa.Column('employer', sa.String(length=63), nullable=True))
        batch_op.add_column(sa.Column('english_assitance', sa.Boolean(), nullable=True))
        batch_op.add_column(sa.Column('fax', sa.String(length=15), nullable=True))
        batch_op.add_column(sa.Column('gender', sa.SmallInteger(), nullable=True))
        batch_op.add_column(sa.Column('internation_contact', sa.String(length=63), nullable=True))
        batch_op.add_column(sa.Column('lang_identifier', sa.String(length=63), nullable=True))
        batch_op.add_column(sa.Column('learner_unique_identifier', sa.String(length=15), nullable=True))
        batch_op.add_column(sa.Column('manager_supervisor', sa.String(length=63), nullable=True))
        batch_op.add_column(sa.Column('middleName', sa.String(length=63), nullable=True))
        batch_op.add_column(sa.Column('note_type', sa.String(length=63), nullable=True))
        batch_op.add_column(sa.Column('optionalId', sa.String(length=31), nullable=True))
        batch_op.add_column(sa.Column('organization', sa.String(length=255), nullable=True))
        batch_op.add_column(sa.Column('payer', sa.String(length=63), nullable=True))
        batch_op.add_column(sa.Column('phone_home', sa.String(length=15), nullable=True))
        batch_op.add_column(sa.Column('phone_work', sa.String(length=15), nullable=True))
        batch_op.add_column(sa.Column('position', sa.String(length=63), nullable=True))
        batch_op.add_column(sa.Column('preferredName', sa.String(length=63), nullable=True))
        batch_op.add_column(sa.Column('primary_postal_addr_building_name', sa.String(length=63), nullable=True))
        batch_op.add_column(sa.Column('primary_postal_addr_city_suburb', sa.String(length=63), nullable=True))
        batch_op.add_column(sa.Column('primary_postal_addr_country', sa.String(length=31), nullable=True))
        batch_op.add_column(sa.Column('primary_postal_addr_po_box', sa.String(length=63), nullable=True))
        batch_op.add_column(sa.Column('primary_postal_addr_post_code', sa.String(length=7), nullable=True))
        batch_op.add_column(sa.Column('primary_postal_addr_state', sa.String(length=7), nullable=True))
        batch_op.add_column(sa.Column('primary_postal_addr_street_no_name', sa.String(length=63), nullable=True))
        batch_op.add_column(sa.Column('primary_postal_addr_unit_details', sa.String(length=63), nullable=True))
        batch_op.add_column(sa.Column('primary_street_addr_building_name', sa.String(length=63), nullable=True))
        batch_op.add_column(sa.Column('primary_street_addr_city_suburb', sa.String(length=63), nullable=True))
        batch_op.add_column(sa.Column('primary_street_addr_country', sa.String(length=31), nullable=True))
        batch_op.add_column(sa.Column('primary_street_addr_po_box', sa.String(length=63), nullable=True))
        batch_op.add_column(sa.Column('primary_street_addr_post_code', sa.String(length=7), nullable=True))
        batch_op.add_column(sa.Column('primary_street_addr_state', sa.String(length=7), nullable=True))
        batch_op.add_column(sa.Column('primary_street_addr_street_no_name', sa.String(length=63), nullable=True))
        batch_op.add_column(sa.Column('primary_street_addr_unit_details', sa.String(length=63), nullable=True))
        batch_op.add_column(sa.Column('relationship', sa.String(length=15), nullable=True))
        batch_op.add_column(sa.Column('sace_student_id', sa.String(length=15), nullable=True))
        batch_op.add_column(sa.Column('section', sa.String(length=63), nullable=True))
        batch_op.add_column(sa.Column('torres_strait_islander_origin', sa.Boolean(), nullable=True))
        batch_op.add_column(sa.Column('unique_std_identifier', sa.String(length=15), nullable=True))
        batch_op.add_column(sa.Column('website', sa.String(length=255), nullable=True))
        batch_op.add_column(sa.Column('workready_participant_no', sa.String(length=15), nullable=True))
        batch_op.drop_column('state')

    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('user', schema=None) as batch_op:
        batch_op.add_column(sa.Column('state', sa.VARCHAR(length=7), nullable=True))
        batch_op.drop_column('workready_participant_no')
        batch_op.drop_column('website')
        batch_op.drop_column('unique_std_identifier')
        batch_op.drop_column('torres_strait_islander_origin')
        batch_op.drop_column('section')
        batch_op.drop_column('sace_student_id')
        batch_op.drop_column('relationship')
        batch_op.drop_column('primary_street_addr_unit_details')
        batch_op.drop_column('primary_street_addr_street_no_name')
        batch_op.drop_column('primary_street_addr_state')
        batch_op.drop_column('primary_street_addr_post_code')
        batch_op.drop_column('primary_street_addr_po_box')
        batch_op.drop_column('primary_street_addr_country')
        batch_op.drop_column('primary_street_addr_city_suburb')
        batch_op.drop_column('primary_street_addr_building_name')
        batch_op.drop_column('primary_postal_addr_unit_details')
        batch_op.drop_column('primary_postal_addr_street_no_name')
        batch_op.drop_column('primary_postal_addr_state')
        batch_op.drop_column('primary_postal_addr_post_code')
        batch_op.drop_column('primary_postal_addr_po_box')
        batch_op.drop_column('primary_postal_addr_country')
        batch_op.drop_column('primary_postal_addr_city_suburb')
        batch_op.drop_column('primary_postal_addr_building_name')
        batch_op.drop_column('preferredName')
        batch_op.drop_column('position')
        batch_op.drop_column('phone_work')
        batch_op.drop_column('phone_home')
        batch_op.drop_column('payer')
        batch_op.drop_column('organization')
        batch_op.drop_column('optionalId')
        batch_op.drop_column('note_type')
        batch_op.drop_column('middleName')
        batch_op.drop_column('manager_supervisor')
        batch_op.drop_column('learner_unique_identifier')
        batch_op.drop_column('lang_identifier')
        batch_op.drop_column('internation_contact')
        batch_op.drop_column('gender')
        batch_op.drop_column('fax')
        batch_op.drop_column('english_assitance')
        batch_op.drop_column('employer')
        batch_op.drop_column('dob')
        batch_op.drop_column('division')
        batch_op.drop_column('country_of_citizenship')
        batch_op.drop_column('country_of_birth')
        batch_op.drop_column('contact_source')
        batch_op.drop_column('contact_number')
        batch_op.drop_column('contact_name')
        batch_op.drop_column('contact_categories')
        batch_op.drop_column('contact_active')
        batch_op.drop_column('comments')
        batch_op.drop_column('coach_mentor')
        batch_op.drop_column('city_of_birth')
        batch_op.drop_column('attending_other_school')
        batch_op.drop_column('alternative_email_address')
        batch_op.drop_column('agent')
        batch_op.drop_column('aboriginal_origin')
        batch_op.drop_column('RACGP_no')

    # ### end Alembic commands ###
