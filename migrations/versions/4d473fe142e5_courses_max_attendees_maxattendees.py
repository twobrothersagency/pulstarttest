"""Courses max_attendees > maxAttendees

Revision ID: 4d473fe142e5
Revises: d3999eb2aea3
Create Date: 2020-08-03 23:35:55.132925

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '4d473fe142e5'
down_revision = 'd3999eb2aea3'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('course', schema=None) as batch_op:
        batch_op.add_column(sa.Column('maxAttendees', sa.Integer(), nullable=True))
        batch_op.drop_column('max_attendees')

    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('course', schema=None) as batch_op:
        batch_op.add_column(sa.Column('max_attendees', sa.INTEGER(), nullable=True))
        batch_op.drop_column('maxAttendees')

    # ### end Alembic commands ###
