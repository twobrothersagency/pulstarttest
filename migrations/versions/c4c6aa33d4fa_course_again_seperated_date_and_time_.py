"""Course again seperated date and time, with datetime as a placeholder

Revision ID: c4c6aa33d4fa
Revises: 3e1aa6a6fb25
Create Date: 2020-08-02 23:22:57.273840

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c4c6aa33d4fa'
down_revision = '3e1aa6a6fb25'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('course', schema=None) as batch_op:
        batch_op.add_column(sa.Column('date', sa.String(length=100), nullable=True))
        batch_op.add_column(sa.Column('endTime', sa.String(length=100), nullable=True))
        batch_op.add_column(sa.Column('startTime', sa.String(length=100), nullable=True))

    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('course', schema=None) as batch_op:
        batch_op.drop_column('startTime')
        batch_op.drop_column('endTime')
        batch_op.drop_column('date')

    # ### end Alembic commands ###
