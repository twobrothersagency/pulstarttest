from flask import render_template
from flask_mail import Message
from threading import Thread
from app import app, mail

def send_async_email(app, msg):
    with app.app_context():
        mail.send(msg)


def send_email(subject, sender, recipients, text_body, html_body, attachments=None, sync=False):
    msg = Message(subject, sender=sender, recipients=recipients)
    msg.body = text_body
    msg.html = html_body
    if attachments:
        for attachment in attachments:
            msg.attach(*attachment)
    if sync:
        mail.send(msg)
    else:
        Thread(target=send_async_email, args=(app, msg)).start()

def send_password_reset_email(user):
    token = user.get_reset_password_token()
    send_email('[Pulsestart] Reset Your Password',
               sender=app.config['ADMINS'][0],
               recipients=[user.email],
               text_body=render_template('email/reset_password.txt',
                                         user=user, token=token),
               html_body=render_template('email/reset_password.html',
                                         user=user, token=token))


def send_password_create_email(user):
    token = user.get_reset_password_token()
    send_email('[Pulsestart] Create Your Password',
               sender=app.config['ADMINS'][0],
               recipients=[user.email],
               text_body=render_template('email/create_password.txt',
                                         user=user, token=token),
               html_body=render_template('email/create_password_email.html',
                                         user=user, token=token))


def send_course_enquiry(current_user, values):
    send_email(values.get('subject'),
        sender=app.config['ADMINS'][0], recipients=[app.config['ADMINS'][0],app.config['ADMINS'][1]],
        text_body=render_template('email/course_enquiry.txt', user=current_user, message=values.get('message')),
        html_body=render_template('email/course_enquiry.html', user=current_user, message=values.get('message')),
        sync=True)

def send_enrol_confirmation(current_user, course):
    date = course.date.strftime("%d/%m/%Y")
    startTime = course.startTime.strftime("%H:%M")
    endTime = course.endTime.strftime("%H:%M")
    # Email enrolee
    send_email(course.title,
        sender=app.config['ADMINS'][0], recipients=[current_user.email],
        text_body=render_template('email/course_enrolment.txt', user=current_user, course=course, date=date, startTime=startTime, endTime=endTime),
        html_body=render_template('email/course_enrolment.html', user=current_user, course=course, date=date, startTime=startTime, endTime=endTime),
        sync=True)

    # Email admin
    send_email(course.title,
        sender=app.config['ADMINS'][0], recipients=[app.config['ADMINS'][0],app.config['ADMINS'][1]],
        text_body=render_template('email/admin_alert_enrolment.txt', user=current_user, course=course, date=date, startTime=startTime, endTime=endTime),
        html_body=render_template('email/admin_alert_enrolment.html', user=current_user, course=course, date=date, startTime=startTime, endTime=endTime),
        sync=True)


def send_admin_inhouse_course_alert(course):
    date = course.date.strftime("%d/%m/%Y")
    startTime = course.startTime.strftime("%H:%M")
    endTime = course.endTime.strftime("%H:%M")
    # Email admin
    send_email(course.title,
        sender=app.config['ADMINS'][0], recipients=[app.config['ADMINS'][0],app.config['ADMINS'][1]],
        text_body=render_template('email/admin_alert_inhouse_course_create.txt', course=course, date=date, startTime=startTime, endTime=endTime),
        html_body=render_template('email/admin_alert_inhouse_course_create.html', course=course, date=date, startTime=startTime, endTime=endTime),
        sync=True)
