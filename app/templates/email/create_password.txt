Hi {{ user.firstName }},

To create your password click on the following link:

{{config.CURRENT_SITE_URL}}/{{ url_for('create_password', token=token, _external=True) }}

Sincerely,

The Pulsestart Team