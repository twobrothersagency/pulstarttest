  // This sample uses the Autocomplete widget to help the user select a
      // place, then it retrieves the address components associated with that
      // place, and then it populates the form fields with those details.
      // This sample requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script
      // src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
      let autocomplete;
      let autocompletepostal;
      const componentForm = {
        street_number: "short_name",
        route: "long_name",
        locality: "long_name",
        administrative_area_level_1: "short_name",
        country: "long_name",
        postal_code: "short_name",
      };
      const componentResolver = {
        street_number: 'street_number',
        route: 'primary_street_addr_street_no_name',
        locality: 'primary_street_addr_city_suburb',
        country: 'primary_street_addr_country',
        administrative_area_level_1: 'primary_street_addr_state',
        postal_code: 'primary_street_addr_post_code',
      };
      const componentResolverPostal = {
        street_number:'postal_street_number',
        route: 'primary_postal_addr_street_no_name',
        locality: 'primary_postal_addr_city_suburb',
        country: 'primary_postal_addr_country',
        administrative_area_level_1: 'primary_postal_addr_state',
        postal_code: 'primary_postal_addr_post_code',
      }


      function initAutocomplete() {
        // Create the autocomplete object, restricting the search predictions to
        // geographical location types.
        autocomplete = new google.maps.places.Autocomplete(
          document.getElementById("autocomplete_address"),
          { types: ["geocode"] }
        );
        //initializing for postal search
        autocompletepostal = new google.maps.places.Autocomplete(
          document.getElementById("autocomplete_postal_address"),
          { types: ["geocode"] }
        );
        // Avoid paying for data that you don't need by restricting the set of
        // place fields that are returned to just the address components.
        autocomplete.setFields(["address_component"]);
        autocompletepostal.setFields(['address_component'])
        // When the user selects an address from the drop-down, populate the
        // address fields in the form.
        autocomplete.addListener("place_changed", fillInAddress);
        autocompletepostal.addListener("place_changed",fillinPostalAddress);
      }
      function fillInAddress() {
        //clearing out dagling values for all fields
        for (const key in componentResolver){
          document.getElementById(componentResolver[key]).value = ''
        }
        // Get the place details from the autocomplete object.
        const place = autocomplete.getPlace();
         for (const component in componentForm) {
        }
        // Get each component of the address from the place details,
        // and then fill-in the corresponding field on the form.
        for (const component of place.address_components) {
          const addressType = component.types[0];
          if (componentForm[addressType]) {
            const val = component[componentForm[addressType]];
            const streetNumber = component[componentForm['street_number']];
            if(addressType=='route'){
               const streetNumber = document.getElementById(componentResolver['street_number']).value + ',';
               document.getElementById(componentResolver[addressType]).value = streetNumber + val;
            }
            else {
               document.getElementById(componentResolver[addressType]).value = val;
            }
          }
        }
      }

      function fillinPostalAddress() {

        //clearing out dagling values for all fields
        for (const key in componentResolverPostal){
          document.getElementById(componentResolverPostal[key]).value = ''
        }


        // Get the place details from the autocomplete object.
        const place = autocompletepostal.getPlace();
         for (const component in componentForm) {
        }
        // Get each component of the address from the place details,
        // and then fill-in the corresponding field on the form.
        for (const component of place.address_components) {
          const addressType = component.types[0];
          
          if (componentForm[addressType]) {
            
            const val = component[componentForm[addressType]];
            const streetNumber = component[componentForm['street_number']];

            if(addressType=='route'){
               const streetNumber = document.getElementById(componentResolverPostal['street_number']).value + ',';
               document.getElementById(componentResolverPostal[addressType]).value = streetNumber + val;
            
            }else {
               document.getElementById(componentResolverPostal[addressType]).value = val;
            }
          }
        }
      }
      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition((position) => {
            const geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude,
            };
            const circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy,
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }