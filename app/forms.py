from flask import request,g
from flask_wtf import FlaskForm
from wtforms.fields.html5 import DateField, TimeField
from wtforms.validators import ValidationError, DataRequired, InputRequired, Email, EqualTo, Length
from wtforms import (
    StringField, PasswordField, BooleanField, SubmitField, TextAreaField, DecimalField, IntegerField,
    TextField, SelectField, RadioField, HiddenField, SelectMultipleField
)

from app.models import User, USER_CITIZENSHIP_STATUS_CHOICES, \
    USER_EMPLOYMENT_STATUS_CHOICES, USER_SCHOOL_COMPLETED_LEVEL_CHOICES, USER_STATE_CHOICES, \
    USER_SURVEY_CONTACT_STATUS_CHOICES, USER_TITLE_CHOICES, USER_ENGLISH_PROFICIENCY_CHOICES, \
    USER_ROLE_CHOICES

YES_NO_CHOICES = [('1', 'Yes'), ('0', 'No')]
DISABILITIES_CHOICES = [('1','Yes'),('2','No'),('3','Not Specified')]

LANAGUE_CHOICES = [
            ("ang","English"),
            ("aar","Afar"),
            ("abk","Abkhazian"),
            ("ace","Achines"),
            ("ach","Acoli"),
            ("ada","Adangme"),
            ("ady","Adyghe Adygei"),
            ("afa","Afro-Asiatic languages"),
            ("afh","Afrihili"),
            ("afr","Afrikaans"),
            ("ain","Ainu"),
            ("aka","Akan"),
            ("akk","Akkadian"),
            ("alb","Albanian"),
            ("ale","Aleut"),
            ("alg","Algonquian languages"),
            ("alt","Southern Altai"),
            ("amh","Amharic"),
            ("anp","Angika"),
            ("apa","Apache languages"),
            ("ara","Arabic"),
            ("arc","Official Aramaic (700-300 BCE) Imperial Aramaic (700-300 BCE)"),
            ("arg","Aragonese"),
            ("arm","Armenian"),
            ("arn","Mapudungun Mapuche"),
            ("arp","Arapaho"),
            ("art","Artificial languages"),
            ("arw","Arawak"),
            ("asm","Assamese"),
            ("ast","Asturian Bable Leonese Asturleonese"),
            ("ath","Athapascan languages"),
            ("aus","Australian languages"),
            ("ava","Avaric"),
            ("ave","Avestan"),
            ("awa","Awadhi"),
            ("aym","Aymara"),
            ("aze","Azerbaijani"),
            ("bad","Banda languages"),
            ("bai","Bamileke languages"),
            ("bak","Bashkir"),
            ("bal","Baluchi"),
            ("bam","Bambara"),
            ("ban","Balinese"),
            ("baq","Basque"),
            ("bas","Basa"),
            ("bat","Baltic languages"),
            ("bej","Beja Bedawiyet"),
            ("bel","Belarusian"),
            ("bem","Bemba"),
            ("ben","Bengali"),
            ("ber","Berber languages"),
            ("bho","Bhojpuri"),
            ("bih","Bihari languages"),
            ("bik","Bikol"),
            ("bin","Bini Edo"),
            ("bis","Bislama"),
            ("bla","Siksika"),
            ("bnt","Bantu (Other)"),
            ("bos","Bosnian"),
            ("bra","Braj"),
            ("bre","Breton"),
            ("btk","Batak languages"),
            ("bua","Buriat"),
            ("bug","Buginese"),
            ("bul","Bulgarian"),
            ("bur","Burmese"),
            ("byn","Blin Bilin"),
            ("cad","Caddo"),
            ("cai","Central American Indian languages"),
            ("car","Galibi Carib"),
            ("cat","Catalan Valencian"),
            ("cau","Caucasian languages"),
            ("ceb","Cebuano"),
            ("cel","Celtic languages"),
            ("cha","Chamorro"),
            ("chb","Chibcha"),
            ("che","Chechen"),
            ("chg","Chagatai"),
            ("chi","Chinese"),
            ("chk","Chuukese"),
            ("chm","Mari"),
            ("chn","Chinook jargon"),
            ("cho","Choctaw"),
            ("chp","Chipewyan Dene Suline"),
            ("chr","Cherokee"),
            ("chu","Church Slavic Old Slavonic Church Slavonic Old Bulgarian Old Church Slavonic"),
            ("chv","Chuvash"),
            ("chy","Cheyenne"),
            ("cmc","Chamic languages"),
            ("cop","Coptic"),
            ("cor","Cornish"),
            ("cos","Corsican"),
            ("cpe","Creoles and pidgins"),
            ("cpf","Creoles and pidgins"),
            ("cpp","Creoles and pidgins"),
            ("cre","Cree"),
            ("crh","Crimean Tatar Crimean Turkish"),
            ("crp","Creoles and pidgins "),
            ("csb","Kashubian"),
            ("cus","Cushitic languages"),
            ("cze","Czech"),
            ("dak","Dakota"),
            ("dan","Danish"),
            ("dar","Dargwa"),
            ("day","Land Dayak languages"),
            ("del","Delaware"),
            ("den","Slave (Athapascan)"),
            ("dgr","Dogrib"),
            ("din","Dinka"),
            ("div","Divehi Dhivehi Maldivian"),
            ("doi","Dogri"),
            ("dra","Dravidian languages"),
            ("dsb","Lower Sorbian"),
            ("dua","Duala"),
            ("dum","Dutch"),
            ("dut","Dutch Flemish"),
            ("dyu","Dyula"),
            ("dzo","Dzongkha"),
            ("efi","Efik"),
            ("egy","Egyptian (Ancient)"),
            ("eka","Ekajuk"),
            ("elx","Elamite"),
            ("eng","English"),
            ("enm","English"),
            ("epo","Esperanto"),
            ("est","Estonian"),
            ("ewe","Ewe"),
            ("ewo","Ewondo"),
            ("fan","Fang"),
            ("fao","Faroese"),
            ("fat","Fanti"),
            ("fij","Fijian"),
            ("fil","Filipino Pilipino"),
            ("fin","Finnish"),
            ("fiu","Finno-Ugrian languages"),
            ("fon","Fon"),
            ("fre","French"),
            ("frm","French"),
            ("fro","French"),
            ("frr","Northern Frisian"),
            ("frs","Eastern Frisian"),
            ("fry","Western Frisian"),
            ("ful","Fulah"),
            ("fur","Friulian"),
            ("gaa","Ga"),
            ("gay","Gayo"),
            ("gba","Gbaya"),
            ("gem","Germanic languages"),
            ("geo","Georgian"),
            ("ger","German"),
            ("gez","Geez"),
            ("gil","Gilbertese"),
            ("gla","Gaelic Scottish Gaelic"),
            ("gle","Irish"),
            ("glg","Galician"),
            ("glv","Manx"),
            ("gmh","German"),
            ("goh","German"),
            ("gon","Gondi"),
            ("gor","Gorontalo"),
            ("got","Gothic"),
            ("grb","Grebo"),
            ("grc","Greek"),
            ("gre","Greek"),
            ("grn","Guarani"),
            ("gsw","Swiss German Alemannic Alsatian"),
            ("guj","Gujarati"),
            ("gwi","Gwich'in"),
            ("hai","Haida"),
            ("hat","Haitian Haitian Creole"),
            ("hau","Hausa"),
            ("haw","Hawaiian"),
            ("heb","Hebrew"),
            ("her","Herero"),
            ("hil","Hiligaynon"),
            ("him","Himachali languages Western Pahari languages"),
            ("hin","Hindi"),
            ("hit","Hittite"),
            ("hmn","Hmong Mong"),
            ("hmo","Hiri Motu"),
            ("hrv","Croatian"),
            ("hsb","Upper Sorbian"),
            ("hun","Hungarian"),
            ("hup","Hupa"),
            ("iba","Iban"),
            ("ibo","Igbo"),
            ("ice","Icelandic"),
            ("ido","Ido"),
            ("iii","Sichuan Yi Nuosu"),
            ("ijo","Ijo languages"),
            ("iku","Inuktitut"),
            ("ile","Interlingue Occidental"),
            ("ilo","Iloko"),
            ("ina","Interlingua (International Auxiliary Language Association)"),
            ("inc","Indic languages"),
            ("ind","Indonesian"),
            ("ine","Indo-European languages"),
            ("inh","Ingush"),
            ("ipk","Inupiaq"),
            ("ira","Iranian languages"),
            ("iro","Iroquoian languages"),
            ("ita","Italian"),
            ("jav","Javanese"),
            ("jbo","Lojban"),
            ("jpn","Japanese"),
            ("jpr","Judeo-Persian"),
            ("jrb","Judeo-Arabic"),
            ("kaa","Kara-Kalpak"),
            ("kab","Kabyle"),
            ("kac","Kachin Jingpho"),
            ("kal","Kalaallisut Greenlandic"),
            ("kam","Kamba"),
            ("kan","Kannada"),
            ("kar","Karen languages"),
            ("kas","Kashmiri"),
            ("kau","Kanuri"),
            ("kaw","Kawi"),
            ("kaz","Kazakh"),
            ("kbd","Kabardian"),
            ("kha","Khasi"),
            ("khi","Khoisan languages"),
            ("khm","Central Khmer"),
            ("kho","Khotanese Sakan"),
            ("kik","Kikuyu Gikuyu"),
            ("kin","Kinyarwanda"),
            ("kir","Kirghiz Kyrgyz"),
            ("kmb","Kimbundu"),
            ("kok","Konkani"),
            ("kom","Komi"),
            ("kon","Kongo"),
            ("kor","Korean"),
            ("kos","Kosraean"),
            ("kpe","Kpelle"),
            ("krc","Karachay-Balkar"),
            ("krl","Karelian"),
            ("kro","Kru languages"),
            ("kru","Kurukh"),
            ("kua","Kuanyama Kwanyama"),
            ("kum","Kumyk"),
            ("kur","Kurdish"),
            ("kut","Kutenai"),
            ("lad","Ladino"),
            ("lah","Lahnda"),
            ("lam","Lamba"),
            ("lao","Lao"),
            ("lat","Latin"),
            ("lav","Latvian"),
            ("lez","Lezghian"),
            ("lim","Limburgan Limburger Limburgish"),
            ("lin","Lingala"),
            ("lit","Lithuanian"),
            ("lol","Mongo"),
            ("loz","Lozi"),
            ("ltz","Luxembourgish Letzeburgesch"),
            ("lua","Luba-Lulua"),
            ("lub","Luba-Katanga"),
            ("lug","Ganda"),
            ("lui","Luiseno"),
            ("lun","Lunda"),
            ("luo","Luo (Kenya and Tanzania)"),
            ("lus","Lushai"),
            ("mac","Macedonian"),
            ("mad","Madurese"),
            ("mag","Magahi"),
            ("mah","Marshallese"),
            ("mai","Maithili"),
            ("mak","Makasar"),
            ("mal","Malayalam"),
            ("man","Mandingo"),
            ("mao","Maori"),
            ("map","Austronesian languages"),
            ("mar","Marathi"),
            ("mas","Masai"),
            ("may","Malay"),
            ("mdf","Moksha"),
            ("mdr","Mandar"),
            ("men","Mende"),
            ("mga","Irish"),
            ("mic","Mi'kmaq Micmac"),
            ("min","Minangkabau"),
            ("mis","Uncoded languages"),
            ("mkh","Mon-Khmer languages"),
            ("mlg","Malagasy"),
            ("mlt","Maltese"),
            ("mnc","Manchu"),
            ("mni","Manipuri"),
            ("mno","Manobo languages"),
            ("moh","Mohawk"),
            ("mon","Mongolian"),
            ("mos","Mossi"),
            ("mul","Multiple languages"),
            ("mun","Munda languages"),
            ("mus","Creek"),
            ("mwl","Mirandese"),
            ("mwr","Marwari"),
            ("myn","Mayan languages"),
            ("myv","Erzya"),
            ("nah","Nahuatl languages"),
            ("nai","North American Indian languages"),
            ("nap","Neapolitan"),
            ("nau","Nauru"),
            ("nav","Navajo Navaho"),
            ("nbl","Ndebele"),
            ("nde","Ndebele"),
            ("ndo","Ndonga"),
            ("nds","Low German Low Saxon German"),
            ("nep","Nepali"),
            ("new","Nepal Bhasa Newari"),
            ("nia","Nias"),
            ("nic","Niger-Kordofanian languages"),
            ("niu","Niuean"),
            ("nno","Norwegian Nynorsk Nynorsk"),
            ("nob","Bokmål"),
            ("nog","Nogai"),
            ("non","Norse"),
            ("nor","Norwegian"),
            ("nqo","N'Ko"),
            ("nso","Pedi Sepedi Northern Sotho"),
            ("nub","Nubian languages"),
            ("nwc","Classical Newari Old Newari Classical Nepal Bhasa"),
            ("nya","Chichewa Chewa Nyanja"),
            ("nym","Nyamwezi"),
            ("nyn","Nyankole"),
            ("nyo","Nyoro"),
            ("nzi","Nzima"),
            ("oci","Occitan (post 1500) Provençal"),
            ("oji","Ojibwa"),
            ("ori","Oriya"),
            ("orm","Oromo"),
            ("osa","Osage"),
            ("oss","Ossetian Ossetic"),
            ("ota","Turkish"),
            ("oto","Otomian languages"),
            ("paa","Papuan languages"),
            ("pag","Pangasinan"),
            ("pal","Pahlavi"),
            ("pam","Pampanga Kapampangan"),
            ("pan","Panjabi Punjabi"),
            ("pap","Papiamento"),
            ("pau","Palauan"),
            ("peo","Persian"),
            ("per","Persian"),
            ("phi","Philippine languages"),
            ("phn","Phoenician"),
            ("pli","Pali"),
            ("pol","Polish"),
            ("pon","Pohnpeian"),
            ("por","Portuguese"),
            ("pra","Prakrit languages"),
            ("pro","Provençal"),
            ("pus","Pushto Pashto"),
            ("qaa","qtz Reserved for local use"),
            ("que","Quechua"),
            ("raj","Rajasthani"),
            ("rap","Rapanui"),
            ("rar","Rarotongan Cook Islands Maori"),
            ("roa","Romance languages"),
            ("roh","Romansh"),
            ("rom","Romany"),
            ("rum","Romanian Moldavian Moldovan"),
            ("run","Rundi"),
            ("rup","Aromanian Arumanian Macedo-Romanian"),
            ("rus","Russian"),
            ("sad","Sandawe"),
            ("sag","Sango"),
            ("sah","Yakut"),
            ("sai","South American Indian (Other)"),
            ("sal","Salishan languages"),
            ("sam","Samaritan Aramaic"),
            ("san","Sanskrit"),
            ("sas","Sasak"),
            ("sat","Santali"),
            ("scn","Sicilian"),
            ("sco","Scots"),
            ("sel","Selkup"),
            ("sem","Semitic languages"),
            ("sga","Irish"),
            ("sgn","Sign Languages"),
            ("shn","Shan"),
            ("sid","Sidamo"),
            ("sin","Sinhala Sinhalese"),
            ("sio","Siouan languages"),
            ("sit","Sino-Tibetan languages"),
            ("sla","Slavic languages"),
            ("slo","Slovak"),
            ("slv","Slovenian"),
            ("sma","Southern Sami"),
            ("sme","Northern Sami"),
            ("smi","Sami languages"),
            ("smj","Lule Sami"),
            ("smn","Inari Sami"),
            ("smo","Samoan"),
            ("sms","Skolt Sami"),
            ("sna","Shona"),
            ("snd","Sindhi"),
            ("snk","Soninke"),
            ("sog","Sogdian"),
            ("som","Somali"),
            ("son","Songhai languages"),
            ("sot","Sotho"),
            ("spa","Spanish Castilian"),
            ("srd","Sardinian"),
            ("srn","Sranan Tongo"),
            ("srp","Serbian"),
            ("srr","Serer"),
            ("ssa","Nilo-Saharan languages"),
            ("ssw","Swati"),
            ("suk","Sukuma"),
            ("sun","Sundanese"),
            ("sus","Susu"),
            ("sux","Sumerian"),
            ("swa","Swahili"),
            ("swe","Swedish"),
            ("syc","Classical Syriac"),
            ("syr","Syriac"),
            ("tah","Tahitian"),
            ("tai","Tai languages"),
            ("tam","Tamil"),
            ("tat","Tatar"),
            ("tel","Telugu"),
            ("tem","Timne"),
            ("ter","Tereno"),
            ("tet","Tetum"),
            ("tgk","Tajik"),
            ("tgl","Tagalog"),
            ("tha","Thai"),
            ("tib","Tibetan"),
            ("tig","Tigre"),
            ("tir","Tigrinya"),
            ("tiv","Tiv"),
            ("tkl","Tokelau"),
            ("tlh","Klingon tlhIngan-Hol"),
            ("tli","Tlingit"),
            ("tmh","Tamashek"),
            ("tog","Tonga (Nyasa)"),
            ("ton","Tonga (Tonga Islands)"),
            ("tpi","Tok Pisin"),
            ("tsi","Tsimshian"),
            ("tsn","Tswana"),
            ("tso","Tsonga"),
            ("tuk","Turkmen"),
            ("tum","Tumbuka"),
            ("tup","Tupi languages"),
            ("tur","Turkish"),
            ("tut","Altaic languages"),
            ("tvl","Tuvalu"),
            ("twi","Twi"),
            ("tyv","Tuvinian"),
            ("udm","Udmurt"),
            ("uga","Ugaritic"),
            ("uig","Uighur Uyghur"),
            ("ukr","Ukrainian"),
            ("umb","Umbundu"),
            ("und","Undetermined"),
            ("urd","Urdu"),
            ("uzb","Uzbek"),
            ("vai","Vai"),
            ("ven","Venda"),
            ("vie","Vietnamese"),
            ("vol","Volapük"),
            ("vot","Votic"),
            ("wak","Wakashan languages"),
            ("wal","Walamo"),
            ("war","Waray"),
            ("was","Washo"),
            ("wel","Welsh"),
            ("wen","Sorbian languages"),
            ("wln","Walloon"),
            ("wol","Wolof"),
            ("xal","Kalmyk Oirat"),
            ("xho","Xhosa"),
            ("yao","Yao"),
            ("yap","Yapese"),
            ("yid","Yiddish"),
            ("yor","Yoruba"),
            ("ypk","Yupik languages"),
            ("zap","Zapotec"),
            ("zbl","Blissymbols Blissymbolics Bliss"),
            ("zen","Zenaga"),
            ("zha","Zhuang Chuang"),
            ("znd","Zande languages"),
            ("zul","Zulu"),
            ("zun","Zuni"),
            ("zxx","No linguistic content Not applicable"),
            ("zza","Zaza Dimili Dimli Kirdki Kirmanjki Zazaki")
        ]

COUNTRY_CHOICES = [
        ("Australia","Australia"),
        ("Afghanistan","Afghanistan"),
        ("Åland Islands","Åland Islands"),
        ("Albania","Albania"),
        ("Algeria","Algeria"),
        ("American Samoa","American Samoa"),
        ("Andorra","Andorra"),
        ("Angola","Angola"),
        ("Anguilla","Anguilla"),
        ("Antarctica","Antarctica"),
        ("Antigua and Barbuda","Antigua and Barbuda"),
        ("Argentina","Argentina"),
        ("Armenia","Armenia"),
        ("Aruba","Aruba"),
        ("Austria","Austria"),
        ("Azerbaijan","Azerbaijan"),
        ("Bahamas","Bahamas"),
        ("Bahrain","Bahrain"),
        ("Bangladesh","Bangladesh"),
        ("Barbados","Barbados"),
        ("Belarus","Belarus"),
        ("Belgium","Belgium"),
        ("Belize","Belize"),
        ("Benin","Benin"),
        ("Bermuda","Bermuda"),
        ("Bhutan","Bhutan"),
        ("Bolivia","Bolivia"),
        ("Bosnia and Herzegovina","Bosnia and Herzegovina"),
        ("Botswana","Botswana"),
        ("Bouvet Island","Bouvet Island"),
        ("Brazil","Brazil"),
        ("British Indian Ocean Territory","British Indian Ocean Territory"),
        ("Brunei Darussalam","Brunei Darussalam"),
        ("Bulgaria","Bulgaria"),
        ("Burkina Faso","Burkina Faso"),
        ("Burundi","Burundi"),
        ("Cambodia","Cambodia"),
        ("Cameroon","Cameroon"),
        ("Canada","Canada"),
        ("Cape Verde","Cape Verde"),
        ("Cayman Islands","Cayman Islands"),
        ("Central African Republic","Central African Republic"),
        ("Chad","Chad"),
        ("Chile","Chile"),
        ("China","China"),
        ("Christmas Island","Christmas Island"),
        ("Cocos (Keeling) Islands","Cocos (Keeling) Islands"),
        ("Colombia","Colombia"),
        ("Comoros","Comoros"),
        ("Congo","Congo"),
        ("Congo, The Democratic Republic of The","Congo, The Democratic Republic of The"),
        ("Cook Islands","Cook Islands"),
        ("Costa Rica","Costa Rica"),
        ("Cote D'ivoire","Cote D'ivoire"),
        ("Croatia","Croatia"),
        ("Cuba","Cuba"),
        ("Cyprus","Cyprus"),
        ("Czech Republic","Czech Republic"),
        ("Denmark","Denmark"),
        ("Djibouti","Djibouti"),
        ("Dominica","Dominica"),
        ("Dominican Republic","Dominican Republic"),
        ("Ecuador","Ecuador"),
        ("Egypt","Egypt"),
        ("El Salvador","El Salvador"),
        ("Equatorial Guinea","Equatorial Guinea"),
        ("Eritrea","Eritrea"),
        ("Estonia","Estonia"),
        ("Ethiopia","Ethiopia"),
        ("Falkland Islands (Malvinas)","Falkland Islands (Malvinas)"),
        ("Faroe Islands","Faroe Islands"),
        ("Fiji","Fiji"),
        ("Finland","Finland"),
        ("France","France"),
        ("French Guiana","French Guiana"),
        ("French Polynesia","French Polynesia"),
        ("French Southern Territories","French Southern Territories"),
        ("Gabon","Gabon"),
        ("Gambia","Gambia"),
        ("Georgia","Georgia"),
        ("Germany","Germany"),
        ("Ghana","Ghana"),
        ("Gibraltar","Gibraltar"),
        ("Greece","Greece"),
        ("Greenland","Greenland"),
        ("Grenada","Grenada"),
        ("Guadeloupe","Guadeloupe"),
        ("Guam","Guam"),
        ("Guatemala","Guatemala"),
        ("Guernsey","Guernsey"),
        ("Guinea","Guinea"),
        ("Guinea-bissau","Guinea-bissau"),
        ("Guyana","Guyana"),
        ("Haiti","Haiti"),
        ("Heard Island and Mcdonald Islands","Heard Island and Mcdonald Islands"),
        ("Holy See (Vatican City State)","Holy See (Vatican City State)"),
        ("Honduras","Honduras"),
        ("Hong Kong","Hong Kong"),
        ("Hungary","Hungary"),
        ("Iceland","Iceland"),
        ("India","India"),
        ("Indonesia","Indonesia"),
        ("Iran, Islamic Republic of","Iran, Islamic Republic of"),
        ("Iraq","Iraq"),
        ("Ireland","Ireland"),
        ("Isle of Man","Isle of Man"),
        ("Israel","Israel"),
        ("Italy","Italy"),
        ("Jamaica","Jamaica"),
        ("Japan","Japan"),
        ("Jersey","Jersey"),
        ("Jordan","Jordan"),
        ("Kazakhstan","Kazakhstan"),
        ("Kenya","Kenya"),
        ("Kiribati","Kiribati"),
        ("Korea, Democratic People's Republic of","Korea, Democratic People's Republic of"),
        ("Korea, Republic of","Korea, Republic of"),
        ("Kuwait","Kuwait"),
        ("Kyrgyzstan","Kyrgyzstan"),
        ("Lao People's Democratic Republic","Lao People's Democratic Republic"),
        ("Latvia","Latvia"),
        ("Lebanon","Lebanon"),
        ("Lesotho","Lesotho"),
        ("Liberia","Liberia"),
        ("Libyan Arab Jamahiriya","Libyan Arab Jamahiriya"),
        ("Liechtenstein","Liechtenstein"),
        ("Lithuania","Lithuania"),
        ("Luxembourg","Luxembourg"),
        ("Macao","Macao"),
        ("Macedonia, The Former Yugoslav Republic of","Macedonia, The Former Yugoslav Republic of"),
        ("Madagascar","Madagascar"),
        ("Malawi","Malawi"),
        ("Malaysia","Malaysia"),
        ("Maldives","Maldives"),
        ("Mali","Mali"),
        ("Malta","Malta"),
        ("Marshall Islands","Marshall Islands"),
        ("Martinique","Martinique"),
        ("Mauritania","Mauritania"),
        ("Mauritius","Mauritius"),
        ("Mayotte","Mayotte"),
        ("Mexico","Mexico"),
        ("Micronesia, Federated States of","Micronesia, Federated States of"),
        ("Moldova, Republic of","Moldova, Republic of"),
        ("Monaco","Monaco"),
        ("Mongolia","Mongolia"),
        ("Montenegro","Montenegro"),
        ("Montserrat","Montserrat"),
        ("Morocco","Morocco"),
        ("Mozambique","Mozambique"),
        ("Myanmar","Myanmar"),
        ("Namibia","Namibia"),
        ("Nauru","Nauru"),
        ("Nepal","Nepal"),
        ("Netherlands","Netherlands"),
        ("Netherlands Antilles","Netherlands Antilles"),
        ("New Caledonia","New Caledonia"),
        ("New Zealand","New Zealand"),
        ("Nicaragua","Nicaragua"),
        ("Niger","Niger"),
        ("Nigeria","Nigeria"),
        ("Niue","Niue"),
        ("Norfolk Island","Norfolk Island"),
        ("Northern Mariana Islands","Northern Mariana Islands"),
        ("Norway","Norway"),
        ("Oman","Oman"),
        ("Pakistan","Pakistan"),
        ("Palau","Palau"),
        ("Palestinian Territory, Occupied","Palestinian Territory, Occupied"),
        ("Panama","Panama"),
        ("Papua New Guinea","Papua New Guinea"),
        ("Paraguay","Paraguay"),
        ("Peru","Peru"),
        ("Philippines","Philippines"),
        ("Pitcairn","Pitcairn"),
        ("Poland","Poland"),
        ("Portugal","Portugal"),
        ("Puerto Rico","Puerto Rico"),
        ("Qatar","Qatar"),
        ("Reunion","Reunion"),
        ("Romania","Romania"),
        ("Russian Federation","Russian Federation"),
        ("Rwanda","Rwanda"),
        ("Saint Helena","Saint Helena"),
        ("Saint Kitts and Nevis","Saint Kitts and Nevis"),
        ("Saint Lucia","Saint Lucia"),
        ("Saint Pierre and Miquelon","Saint Pierre and Miquelon"),
        ("Saint Vincent and The Grenadines","Saint Vincent and The Grenadines"),
        ("Samoa","Samoa"),
        ("San Marino","San Marino"),
        ("Sao Tome and Principe","Sao Tome and Principe"),
        ("Saudi Arabia","Saudi Arabia"),
        ("Senegal","Senegal"),
        ("Serbia","Serbia"),
        ("Seychelles","Seychelles"),
        ("Sierra Leone","Sierra Leone"),
        ("Singapore","Singapore"),
        ("Slovakia","Slovakia"),
        ("Slovenia","Slovenia"),
        ("Solomon Islands","Solomon Islands"),
        ("Somalia","Somalia"),
        ("South Africa","South Africa"),
        ("South Georgia and The South Sandwich Islands","South Georgia and The South Sandwich Islands"),
        ("Spain","Spain"),
        ("Sri Lanka","Sri Lanka"),
        ("Sudan","Sudan"),
        ("Suriname","Suriname"),
        ("Svalbard and Jan Mayen","Svalbard and Jan Mayen"),
        ("Swaziland","Swaziland"),
        ("Sweden","Sweden"),
        ("Switzerland","Switzerland"),
        ("Syrian Arab Republic","Syrian Arab Republic"),
        ("Taiwan, Province of China","Taiwan, Province of China"),
        ("Tajikistan","Tajikistan"),
        ("Tanzania, United Republic of","Tanzania, United Republic of"),
        ("Thailand","Thailand"),
        ("Timor-leste","Timor-leste"),
        ("Togo","Togo"),
        ("Tokelau","Tokelau"),
        ("Tonga","Tonga"),
        ("Trinidad and Tobago","Trinidad and Tobago"),
        ("Tunisia","Tunisia"),
        ("Turkey","Turkey"),
        ("Turkmenistan","Turkmenistan"),
        ("Turks and Caicos Islands","Turks and Caicos Islands"),
        ("Tuvalu","Tuvalu"),
        ("Uganda","Uganda"),
        ("Ukraine","Ukraine"),
        ("United Arab Emirates","United Arab Emirates"),
        ("United Kingdom","United Kingdom"),
        ("United States","United States"),
        ("United States Minor Outlying Islands","United States Minor Outlying Islands"),
        ("Uruguay","Uruguay"),
        ("Uzbekistan","Uzbekistan"),
        ("Vanuatu","Vanuatu"),
        ("Venezuela","Venezuela"),
        ("Viet Nam","Viet Nam"),
        ("Virgin Islands, British","Virgin Islands, British"),
        ("Virgin Islands, U.S.","Virgin Islands, U.S."),
        ("Wallis and Futuna","Wallis and Futuna"),
        ("Western Sahara","Western Sahara"),
        ("Yemen","Yemen"),
        ("Zambia","Zambia"),
        ("Zimbabwe","Zimbabwe")
]

class CurrentPageMixin:
    page = HiddenField(default='1')

class ActionMixin:
    next_page = SubmitField('Next')

class LoginForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')

class PersonalDetailsForm(FlaskForm,CurrentPageMixin):
    #Personal Details
    title = SelectField(choices=USER_TITLE_CHOICES, validators=[DataRequired()])
    firstName = StringField('First Name', validators=[DataRequired()])
    middleName = StringField('Middle Name', validators=[])
    lastName = StringField('Last Name', validators=[DataRequired()])
    preferredName = StringField('Preferred Name', validators=[])
    dob = DateField("Date of Birth")
    mobile = StringField(
        default="0412345678",
        validators=[DataRequired(), Length(min=10, max=10, message="Australian mobile numbers are 10 characters.")]
    )
    email = StringField('Email', validators=[DataRequired(), Email()])
    USI = StringField('Unique Student Identifier')
    usi_exempt = BooleanField("USI Exempt")
    submit = SubmitField('Next')


    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError('Please use a different email address.')

    def validate(self):
        if not super(PersonalDetailsForm, self).validate():
            return False
        if self.usi_exempt:
            if not self.usi_exempt.data:
                user = User.query.filter_by(USI=self.USI.data).first()
                if user:
                    msg = 'Please check your USI is correct.'
                    self.USI.errors.append(msg)
                    return False
                elif len(self.USI.data) != 10:
                    msg = "USI's are 10 characters."
                    self.USI.errors.append(msg)
                    return False
        else:
            user = User.query.filter_by(USI=self.USI.data).first()
            if user:
                msg = 'Please check your USI is correct.'
                self.USI.errors.append(msg)
                return False
            elif len(self.USI.data) != 10:
                msg = "USI's are 10 characters."
                self.USI.errors.append(msg)
                return False
        return True

class AddressDetailsForm(FlaskForm,CurrentPageMixin):
    #Address Details
    autocomplete_address = StringField("Enter your Address")
    primary_street_addr_building_name = StringField("(Primary Street Address) Building name")
    primary_street_addr_unit_details = StringField("(Primary Street Address) Unit Details")
    primary_street_addr_street_no_name = StringField("(Primary Street Address) Street Number/Name")
    primary_street_addr_po_box = StringField("(Primary Street Address) PO Box")
    primary_street_addr_city_suburb = StringField("(Primary Street Address) City/Suburb", validators=[DataRequired()])
    primary_street_addr_state = StringField("(Primary Street Address) State", validators=[DataRequired()])
    primary_street_addr_post_code = StringField("(Primary Street Address) Post code", validators=[DataRequired()])
    primary_street_addr_country = StringField("(Primary Street Address) Country", validators=[DataRequired()])
    street_number = HiddenField()

    autocomplete_postal_address = StringField("Enter your Postal address")
    primary_postal_addr_building_name = StringField("(Primary Postal Address) Building name")
    primary_postal_addr_unit_details = StringField("(Primary Postal Address) Unit Details")
    primary_postal_addr_street_no_name = StringField("(Primary Postal Address) Street Number/Name")
    primary_postal_addr_po_box = StringField("(Primary Postal Address) PO Box")
    primary_postal_addr_city_suburb = StringField("(Primary Postal Address) City/Suburb", validators=[DataRequired()])
    primary_postal_addr_state = StringField("(Primary Postal Address) State", validators=[DataRequired()])
    primary_postal_addr_post_code = StringField("(Primary Postal Address) Post code", validators=[DataRequired()])
    primary_postal_addr_country = StringField("(Primary Postal Address) Country", validators=[DataRequired()])
    postal_street_number = HiddenField()
    
    submit = SubmitField('Next')


class EducationDetailsForm(FlaskForm,CurrentPageMixin):
    #Vocational Education Details
    gender = SelectField(choices=[('1', 'Male'), ('2', 'Female'), ('3', 'Other')], validators=[DataRequired()])
    country_of_birth = SelectField("Country of birth",choices=COUNTRY_CHOICES, validators=[DataRequired()])
    disabilities = RadioField('Disabilities',choices=DISABILITIES_CHOICES, validators=[DataRequired()])
    city_of_birth = StringField("City of birth", validators=[DataRequired()])
    country_of_citizenship = SelectField("Country of citizenship",choices=COUNTRY_CHOICES, validators=[DataRequired()])
    citizenship_status = SelectField(choices=USER_CITIZENSHIP_STATUS_CHOICES, validators=[DataRequired()])
    aboriginal_origin = RadioField("Aboriginal Origin", choices=YES_NO_CHOICES)
    torres_strait_islander_origin = RadioField("Torres Strait Islander Origin", choices=YES_NO_CHOICES)
    employment_status = SelectField(choices=USER_EMPLOYMENT_STATUS_CHOICES, render_kw={'class': 'chosen-select'}, validators=[DataRequired()])
    lang_identifier = SelectField("Language Spoken at Home (Language Identifier)",choices=LANAGUE_CHOICES, validators=[DataRequired()])
    english_proficiency = SelectField(choices=USER_ENGLISH_PROFICIENCY_CHOICES, validators=[DataRequired()])
    english_assistance = RadioField("English Assistance", choices=YES_NO_CHOICES)
    attending_other_school = RadioField("Attending Other School", choices=YES_NO_CHOICES)
    school_completed_level = SelectField(choices=USER_SCHOOL_COMPLETED_LEVEL_CHOICES, validators=[DataRequired()])
    survey_contact_status = SelectField(choices=USER_SURVEY_CONTACT_STATUS_CHOICES,
                                        render_kw={'class': 'chosen-select'})
    RACGP_no = StringField(" (Registered Doctors Only) RACGP Number")
    submit = SubmitField('Next')

class EmergenceyContactForm(FlaskForm,CurrentPageMixin):
    #Emergency Contact Details
    contact_name = StringField(" Emergency Contact Name", validators=[DataRequired()])
    relationship = StringField(" Emergency Relationship", validators=[DataRequired()])
    contact_number = StringField(" Emergency Contact Number", validators=[DataRequired()])
    submit = SubmitField('Next')

class UserCredentialForm(FlaskForm,CurrentPageMixin):
    #Set Password
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField('Repeat Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Register')
    submit = SubmitField('Submit')

class UserRegistrationAdminOnlyForm(FlaskForm,CurrentPageMixin):
    #Admin only
    comments = TextField()
    contact_active = RadioField('Contact Active', choices=[('1', 'Active'), ('0', 'Inactive')])
    submit = SubmitField('Submit')

class RegistrationForm(FlaskForm):
    #Personal Details
    title = SelectField(choices=USER_TITLE_CHOICES, validators=[DataRequired()])
    firstName = StringField('First Name', validators=[DataRequired()])
    middleName = StringField('Middle Name', validators=[])
    lastName = StringField('Last Name', validators=[DataRequired()])
    preferredName = StringField('Preferred Name', validators=[])
    dob = DateField("Date of Birth")
    mobile = StringField(
        default="0412345678",
        validators=[DataRequired(), Length(min=10, max=10, message="Australian mobile numbers are 10 characters.")]
    )
    email = StringField('Email', validators=[DataRequired(), Email()])
    USI = StringField('Unique Student Identifier', validators=[DataRequired()])
    usi_exempt = BooleanField("USI Exempt")

    #Address Details
    autocomplete_address = StringField("Enter your Address")
    primary_street_addr_building_name = StringField("(Primary Street Address) Building name")
    primary_street_addr_unit_details = StringField("(Primary Street Address) Unit Details")
    primary_street_addr_street_no_name = StringField("(Primary Street Address) Street Number/Name")
    primary_street_addr_po_box = StringField("(Primary Street Address) PO Box")
    primary_street_addr_city_suburb = StringField("(Primary Street Address) City/Suburb", validators=[DataRequired()])
    primary_street_addr_state = StringField("(Primary Street Address) State", validators=[DataRequired()])
    primary_street_addr_post_code = StringField("(Primary Street Address) Post code", validators=[DataRequired()])
    primary_street_addr_country = StringField("(Primary Street Address) Country", validators=[DataRequired()])
    street_number = HiddenField("Street Number")

    autocomplete_postal_address = StringField("Enter your Postal address")
    primary_postal_addr_building_name = StringField("(Primary Postal Address) Building name")
    primary_postal_addr_unit_details = StringField("(Primary Postal Address) Unit Details")
    primary_postal_addr_street_no_name = StringField("(Primary Postal Address) Street Number/Name")
    primary_postal_addr_po_box = StringField("(Primary Postal Address) PO Box")
    primary_postal_addr_city_suburb = StringField("(Primary Postal Address) City/Suburb", validators=[DataRequired()])
    primary_postal_addr_state = StringField("(Primary Postal Address) State", validators=[DataRequired()])
    primary_postal_addr_post_code = StringField("(Primary Postal Address) Post code", validators=[DataRequired()])
    primary_postal_addr_country = StringField("(Primary Postal Address) Country", validators=[DataRequired()])
    postal_street_number = HiddenField("Postal Street Number")

    #Vocational Education Details
    gender = SelectField(choices=[('1', 'Male'), ('2', 'Female'), ('3', 'Other')], validators=[DataRequired()])
    country_of_birth = SelectField("Country of birth",choices=COUNTRY_CHOICES, validators=[DataRequired()])
    disabilities = RadioField('Disabilities',choices=DISABILITIES_CHOICES, validators=[DataRequired()])
    city_of_birth = StringField("City of birth", validators=[DataRequired()])
    country_of_citizenship = SelectField("Country of citizenship",choices=COUNTRY_CHOICES, validators=[DataRequired()])
    citizenship_status = SelectField(choices=USER_CITIZENSHIP_STATUS_CHOICES, validators=[DataRequired()])
    aboriginal_origin = RadioField("Aboriginal Origin", choices=YES_NO_CHOICES)
    torres_strait_islander_origin = RadioField("Torres Strait Islander Origin", choices=YES_NO_CHOICES)
    employment_status = SelectField(choices=USER_EMPLOYMENT_STATUS_CHOICES, render_kw={'class': 'chosen-select'}, validators=[DataRequired()])
    lang_identifier = SelectField("Language Spoken at Home (Language Identifier)",choices=LANAGUE_CHOICES, validators=[DataRequired()])
    english_proficiency = SelectField(choices=USER_ENGLISH_PROFICIENCY_CHOICES, validators=[DataRequired()])
    english_assistance = RadioField("English Assistance", choices=YES_NO_CHOICES)
    attending_other_school = RadioField("Attending Other School", choices=YES_NO_CHOICES)
    school_completed_level = SelectField(choices=USER_SCHOOL_COMPLETED_LEVEL_CHOICES, validators=[DataRequired()])
    survey_contact_status = SelectField(choices=USER_SURVEY_CONTACT_STATUS_CHOICES,
                                        render_kw={'class': 'chosen-select'})
    RACGP_no = StringField(" (Registered Doctors Only) RACGP Number")
    
    #Emergency Contact Details
    contact_name = StringField(" Emergency Contact Name", validators=[DataRequired()])
    relationship = StringField(" Emergency Relationship", validators=[DataRequired()])
    contact_number = StringField(" Emergency Contact Number", validators=[DataRequired()])

    #Admin only
    comments = TextField()
    contact_active = RadioField('Contact Active', choices=[('1', 'Active'), ('0', 'Inactive')])

    #Set Password
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField('Repeat Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Register')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError('Please use a different email address.')

    # def validate_USI(self, USI):
    #     # Check USI is unique, and validate USI againt the ADK/API
    #
    #     # TODO - Validate USI against ADK/API
    #     user = User.query.filter_by(USI=USI.data).first()
    #     if user is not None:
    #         raise ValidationError('Please check your USI is correct.')
    def validate(self):
        if not super(RegistrationForm, self).validate():
            return False
        if self.usi_exempt:
            if not self.usi_exempt.data:
                user = User.query.filter_by(USI=self.USI.data).first()
                if user:
                    msg = 'Please check your USI is correct.'
                    self.USI.errors.append(msg)
                    return False
                elif len(self.USI.data) != 10:
                    msg = "USI's are 10 characters."
                    self.USI.errors.append(msg)
                    return False
        return True

    class Meta(FlaskForm.Meta):

        def render_field(self, field, render_kw):
            other_kw = getattr(field, 'render_kw', None)
            if other_kw is not None:
                class1 = other_kw.get('class', None)
                class2 = render_kw.get('class', None)
                if class1 and class2:
                    render_kw['class'] = class2 + ' ' + class1
                render_kw = dict(other_kw, **render_kw)
            return field.widget(field, **render_kw)


class AdminCreateUserForm(FlaskForm):
    title = SelectField(choices=USER_TITLE_CHOICES, validators=[DataRequired()])
    firstName = StringField('First Name', validators=[DataRequired()])
    middleName = StringField('Middle Name', validators=[])
    lastName = StringField('Last Name', validators=[DataRequired()])
    preferredName = StringField('Preferred Name', validators=[])
    dob = DateField("Date of Birth")
    disabilities = RadioField('Disabilities',choices=DISABILITIES_CHOICES)
    mobile = StringField(
        default="0412345678",
        validators=[DataRequired(), Length(min=10, max=10, message="Australian mobile numbers are 10 characters.")]
    )
    email = StringField('Email', validators=[DataRequired(), Email()])

    autocomplete_address = StringField("Enter your Address")
    primary_street_addr_building_name = StringField("(Primary Street Address) Building name")
    primary_street_addr_unit_details = StringField("(Primary Street Address) Unit Details")
    primary_street_addr_street_no_name = StringField("(Primary Street Address) Street Number/Name",
                                                     validators=[DataRequired()])
    primary_street_addr_po_box = StringField("(Primary Street Address) PO Box")
    primary_street_addr_city_suburb = StringField("(Primary Street Address) City/Suburb", validators=[DataRequired()])
    primary_street_addr_state = StringField("(Primary Street Address) State", validators=[DataRequired()])
    primary_street_addr_post_code = StringField("(Primary Street Address) Post code", validators=[DataRequired()])
    primary_street_addr_country = StringField("(Primary Street Address) Country", validators=[DataRequired()])
    street_number = HiddenField("Street Number")

    primary_postal_addr_building_name = StringField("(Primary Postal Address) Building name")
    primary_postal_addr_unit_details = StringField("(Primary Postal Address) Unit Details")
    primary_postal_addr_street_no_name = StringField("(Primary Postal Address) Street Number/Name",
                                                     validators=[DataRequired()])
    primary_postal_addr_po_box = StringField("(Primary Postal Address) PO Box")
    primary_postal_addr_city_suburb = StringField("(Primary Postal Address) City/Suburb", validators=[DataRequired()])
    primary_postal_addr_state = StringField("(Primary Postal Address) State", validators=[DataRequired()])
    primary_postal_addr_post_code = StringField("(Primary Postal Address) Post code", validators=[DataRequired()])
    primary_postal_addr_country = StringField("(Primary Postal Address) Country", validators=[DataRequired()])

    RACGP_no = StringField(" (Registered Doctors Only) RACGP Number")
    gender = SelectField(choices=[('1', 'Male'), ('2', 'Female'), ('3', 'Other')])
    country_of_birth = SelectField("Country of birth",choices=COUNTRY_CHOICES, validators=[DataRequired()])
    city_of_birth = StringField("City of birth")
    country_of_citizenship = SelectField("Country of citizenship",choices=COUNTRY_CHOICES, validators=[DataRequired()])
    citizenship_status = SelectField(choices=USER_CITIZENSHIP_STATUS_CHOICES, validators=[DataRequired()])
    aboriginal_origin = RadioField("Aboriginal Origin", choices=YES_NO_CHOICES)
    torres_strait_islander_origin = RadioField("Torres Strait Islander Origin", choices=YES_NO_CHOICES)
    employment_status = SelectField(choices=USER_EMPLOYMENT_STATUS_CHOICES, render_kw={'class': 'chosen-select'})
    lang_identifier = SelectField("Language Spoken at Home (Language Identifier)",choices=LANAGUE_CHOICES, validators=[DataRequired()])
    english_proficiency = SelectField(choices=USER_ENGLISH_PROFICIENCY_CHOICES, validators=[DataRequired()])
    english_assistance = RadioField("English Assistance", choices=YES_NO_CHOICES)
    attending_other_school = RadioField("Attending Other School", choices=YES_NO_CHOICES)
    school_completed_level = SelectField(choices=USER_SCHOOL_COMPLETED_LEVEL_CHOICES, validators=[DataRequired()])
    survey_contact_status = SelectField(choices=USER_SURVEY_CONTACT_STATUS_CHOICES,
                                        render_kw={'class': 'chosen-select'})


    contact_name = StringField("Emergency Contact Name")
    relationship = StringField("Emergency Relationship")
    contact_number = StringField(" Emergency Contact Number")

    comments = TextField()
    contact_active = RadioField('Contact Active', choices=[('1', 'Active'), ('0', 'Inactive')])

    USI = StringField('Unique Student Identifier')
    usi_exempt = BooleanField("Usi Exempt")

    urole = SelectField(choices=USER_ROLE_CHOICES, render_kw={'class': 'chosen-select'}, validators=[DataRequired()])

    submit = SubmitField('Register')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError('Please use a different email address.')

    # def validate_USI(self, USI):
    #     print('usiii',USI.data)
    #     # Check USI is unique, and validate USI againt the ADK/API
    #
    #     # TODO - Validate USI against ADK/API
    #     user = User.query.filter_by(USI=USI.data).first()
    #     if user is not None:
    #         raise ValidationError('Please check your USI is correct.')
    def validate(self):
        if not super(AdminCreateUserForm, self).validate():
            return False
        if not self.usi_exempt.data:
            user = User.query.filter_by(USI=self.USI.data).first()
            if user:
                msg = 'Please check your USI is correct.'
                self.USI.errors.append(msg)
                return False
            elif len(self.USI.data) != 10:
                msg = "USI's are 10 characters."
                self.USI.errors.append(msg)
                return False
        return True

    class Meta(FlaskForm.Meta):

        def render_field(self, field, render_kw):
            other_kw = getattr(field, 'render_kw', None)
            if other_kw is not None:
                class1 = other_kw.get('class', None)
                class2 = render_kw.get('class', None)
                if class1 and class2:
                    render_kw['class'] = class2 + ' ' + class1
                render_kw = dict(other_kw, **render_kw)
            return field.widget(field, **render_kw)


class EditUserForm(FlaskForm):
    title = SelectField(choices=USER_TITLE_CHOICES, validators=[DataRequired()])
    firstName = StringField('First Name', validators=[DataRequired()])
    middleName = StringField('Middle Name', validators=[])
    lastName = StringField('Last Name', validators=[DataRequired()])
    preferredName = StringField('Preferred Name', validators=[])
    dob = DateField("Date of Birth")
    disabilities = RadioField('Disabilities',choices=DISABILITIES_CHOICES)
    mobile = StringField(
        default="0412345678",
        validators=[DataRequired(), Length(min=10, max=10, message="Australian mobile numbers are 10 characters.")]
    )
    autocomplete_address = StringField("Enter your Address")
    primary_street_addr_building_name = StringField("(Primary Street Address) Building name")
    primary_street_addr_unit_details = StringField("(Primary Street Address) Unit Details")
    primary_street_addr_street_no_name = StringField("(Primary Street Address) Street Number/Name")
    primary_street_addr_po_box = StringField("(Primary Street Address) PO Box")
    primary_street_addr_city_suburb = StringField("(Primary Street Address) City/Suburb")
    primary_street_addr_state = StringField()
    primary_street_addr_post_code = StringField("(Primary Street Address) Post code")
    primary_street_addr_country = StringField("(Primary Street Address) Country")
    street_number = HiddenField("Street Number")

    autocomplete_postal_address = StringField("Enter your Postal Address")
    primary_postal_addr_building_name = StringField("(Primary Postal Address) Building name")
    primary_postal_addr_unit_details = StringField("(Primary Postal Address) Unit Details")
    primary_postal_addr_street_no_name = StringField("(Primary Postal Address) Street Number/Name")
    primary_postal_addr_po_box = StringField("(Primary Postal Address) PO Box")
    primary_postal_addr_city_suburb = StringField("(Primary Postal Address) City/Suburb")
    primary_postal_addr_state = StringField()
    primary_postal_addr_post_code = StringField("(Primary Postal Address) Post code")
    primary_postal_addr_country = StringField("(Primary Postal Address) Country")
    postal_street_number = HiddenField("Street Number")

    RACGP_no = StringField(" (Registered Doctors Only) RACGP Number")
    #
    gender = SelectField(choices=[('1', 'Male'), ('2', 'Female'), ('3', 'Other')])
    country_of_birth = SelectField("Country of birth",choices=COUNTRY_CHOICES, validators=[DataRequired()])
    city_of_birth = StringField("City of birth")
    country_of_citizenship = SelectField("Country of citizenship",choices=COUNTRY_CHOICES, validators=[DataRequired()])
    citizenship_status = SelectField(choices=USER_CITIZENSHIP_STATUS_CHOICES, validators=[DataRequired()])
    aboriginal_origin = RadioField("Aboriginal Origin", choices=YES_NO_CHOICES)
    torres_strait_islander_origin = RadioField("Torres Strait Islander Origin", choices=YES_NO_CHOICES)
    employment_status = SelectField(choices=USER_EMPLOYMENT_STATUS_CHOICES, render_kw={'class': 'chosen-select'})
    lang_identifier = SelectField("Language Spoken at Home (Language Identifier)",choices=LANAGUE_CHOICES, validators=[DataRequired()])
    english_proficiency = SelectField(choices=USER_ENGLISH_PROFICIENCY_CHOICES, validators=[DataRequired()])
    english_assistance = RadioField("English Assistance", choices=YES_NO_CHOICES)
    attending_other_school = RadioField("Attending Other School", choices=YES_NO_CHOICES)
    school_completed_level = SelectField(choices=USER_SCHOOL_COMPLETED_LEVEL_CHOICES, validators=[DataRequired()])
    survey_contact_status = SelectField(choices=USER_SURVEY_CONTACT_STATUS_CHOICES,
                                        render_kw={'class': 'chosen-select'})


    contact_name = StringField("Emergencey Contact Name")
    relationship = StringField("Emergencey Relationship")
    contact_number = StringField("Emergencey Contact Number")

    agent = StringField()
    comments = TextField()
    contact_active = RadioField('Contact Active', choices=[('1', 'Active'), ('0', 'Inactive')])
    urole = SelectField('User Role',choices=[('Admin','Admin'),('Instructor','Instructor'),('Client','Client')])
    submit = SubmitField('Update')

    class Meta(FlaskForm.Meta):
        def render_field(self, field, render_kw):
            other_kw = getattr(field, 'render_kw', None)
            if other_kw is not None:
                class1 = other_kw.get('class', None)
                class2 = render_kw.get('class', None)
                if class1 and class2:
                    render_kw['class'] = class2 + ' ' + class1
                render_kw = dict(other_kw, **render_kw)
            return field.widget(field, **render_kw)


"""
Orig AdminCreateUserForm
class AdminCreateUserForm(FlaskForm):
    firstName = StringField('First Name', validators=[DataRequired()])
    lastName = StringField('Last Name', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    mobile = StringField(default="0412345678", validators=[DataRequired(), Length(min=10, max=10,
                                                                                  message="Australian mobile numbers are 10 characters.")])
    USI = StringField('USI', validators=[DataRequired(), Length(min=10, max=10, message="USI's are 10 characters.")])
    submit = SubmitField('Register')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError('Please use a different email address.')

    def validate_USI(self, USI):
        # Check USI is unique, and validate USI againt the ADK/API

        # TODO - Validate USI against ADK/API
        user = User.query.filter_by(USI=USI.data).first()
        if user is not None:
            raise ValidationError('Please check your USI is correct.')
"""

class EditProfileForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired()])
    about_me = TextAreaField('About me', validators=[Length(min=0, max=140)])
    submit = SubmitField('Submit')

    def __init__(self, original_email, *args, **kwargs):
        super(EditProfileForm, self).__init__(*args, **kwargs)
        self.original_email = original_email

    def validate_email(self, email):
        if email.data != self.original_email:
            user = User.query.filter_by(email=self.email.data).first()
            if user is not None:
                raise ValidationError('Please use a different email.')

class EmptyForm(FlaskForm):
    submit = SubmitField('Submit')

class PostForm(FlaskForm):
    post = TextAreaField('Say something', validators=[
        DataRequired(), Length(min=1, max=140)])
    submit = SubmitField('Submit')

class CourseForm(FlaskForm):
    title = StringField("What's the name of the course?", validators=[
        DataRequired(), Length(min=1, max=140)])
    inhouse = BooleanField("In House Course Offering - Won't be displayed publicly")
    course_code = StringField("Course Code", validators=[
        DataRequired(), Length(min=1, max=140)])
    instructor = SelectField('Select Course Instructor',choices=[])
    date = DateField('Date', format='%Y-%m-%d')
    startTime = TimeField('Start Time')
    endTime = TimeField('End Time')
    close_course_before_start = IntegerField("How many days before course start should enrollment be closed?",
        default="7", validators=[DataRequired()])
    venue = StringField("Where will the course be held?", validators=[
        DataRequired(), Length(min=1, max=140)])
    cost = DecimalField('Cost: AUD$', places=2, rounding=None, validators=[InputRequired()])
    maxAttendees = IntegerField('Maximum number of attendees', validators=[
        DataRequired()])
    quiz_template = SelectMultipleField('Select Quiz Template',choices=[])
    description = TextAreaField("Tell us more about the course", validators=[
        DataRequired(), Length(min=1, max=1200)])
    submit = SubmitField('Submit')

    def validate(self):
        if not FlaskForm.validate(self):
            return False
        if self.startTime.data >= self.endTime.data:
            self.endTime.errors.append('End Time must be after Start Time.')
            return False
        if self.close_course_before_start.data < 0 or self.close_course_before_start.data > 100:
            self.close_course_before_start.errors.append('Course can only be closed to enrollment between 0 and 100 days before Start Date.')
            return False
        return True

class TemplateForm(FlaskForm):
    title = StringField("What's the name of the course?", validators=[
        DataRequired(), Length(min=1, max=140)])
    venue = StringField("Where will the course be held?", validators=[
        DataRequired(), Length(min=1, max=140)])
    cost = DecimalField('Cost: AUD$', places=2, rounding=None, validators=[DataRequired()])
    maxAttendees = IntegerField('Maximum number of attendees', validators=[
        DataRequired()])
    quiz_template = SelectMultipleField('Select Quiz Template',validators=[DataRequired()])
    description = TextAreaField("Tell us more about the course", validators=[
        DataRequired(), Length(min=1, max=1200)])
    submit = SubmitField('Submit')

class ContactForm(FlaskForm):
    name = StringField("Name", render_kw={'readonly': True})
    email = TextField("Email", render_kw={'readonly': True})
    subject = TextField("Subject", render_kw={'readonly': True})
    message = TextAreaField("Message")
    submit = SubmitField("Send")


class ResetPasswordRequestForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email()])
    submit = SubmitField('Request Password Reset')


class ResetPasswordForm(FlaskForm):
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField(
        'Repeat Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Request Password Reset')

class ChangePasswordForm(FlaskForm):
    oldpassword = PasswordField('Old Password',validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField(
        'Repeat Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Change Password')

    def validate_oldpassword(form,field):
        if not g.current_user.check_password(field.data):
            raise ValidationError('Incorrect Old Password')


class CreatePasswordForm(FlaskForm):
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField(
        'Repeat Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Create Password')

class SearchForm(FlaskForm):
    q = StringField(('Search'), validators=[DataRequired()])

    def __init__(self, *args, **kwargs):
        if 'formdata' not in kwargs:
            kwargs['formdata'] = request.args
        if 'csrf_enabled' not in kwargs:
            kwargs['csrf_enabled'] = False
        super(SearchForm, self).__init__(*args, **kwargs)

class MessageForm(FlaskForm):
    message = TextAreaField(('Message'), validators=[
        DataRequired(), Length(min=0, max=140)])
    submit = SubmitField('Submit')

def DynamicQuizForm(questions,page):
    class StaticForm(FlaskForm):
        pass

    for question in questions:
        setattr(
                StaticForm,
                question['id'],
                RadioField(

                    question['question'],
                    choices=question['choices'],
                    validators=[DataRequired()]
                )
        )

        setattr(
                StaticForm,
                'page',
                HiddenField(
                    default=str(page)
                )
        )

    setattr(StaticForm,'submit',SubmitField('Next'))

    return StaticForm()
