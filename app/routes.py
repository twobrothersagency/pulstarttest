import json
from functools import wraps
from datetime import datetime
from dateutil.relativedelta import relativedelta
import os
from uuid import uuid4
import pickle
import warnings
warnings.filterwarnings("ignore")

from flask import jsonify,session
from werkzeug.urls import url_parse
from flask import render_template, flash, redirect, url_for, request, g,session
from flask_login import current_user, login_user, logout_user, login_required
from wtforms import RadioField
from sqlalchemy import or_,sql,and_
from sqlalchemy import func
from sqlalchemy.orm.session import make_transient
import warnings
from app import app, db
from app.models import User, Post, Message, Notification, Course, Template, CourseAttendees,Quiz,QuizResult,\
    QuizTemplate
from app.forms import LoginForm, RegistrationForm, EditProfileForm, EditProfileForm, EmptyForm, PostForm, \
    ResetPasswordRequestForm, ResetPasswordForm, SearchForm, MessageForm, SearchForm, MessageForm, CourseForm,\
    TemplateForm, ContactForm, AdminCreateUserForm, CreatePasswordForm, EditUserForm,ChangePasswordForm,\
    AddressDetailsForm,PersonalDetailsForm,EducationDetailsForm,EmergenceyContactForm,DynamicQuizForm,\
    UserCredentialForm,UserRegistrationAdminOnlyForm
from app.tasks import send_email_to_attendees
from app.email import send_password_create_email, send_enrol_confirmation, send_course_enquiry,\
    send_admin_inhouse_course_alert, send_password_reset_email

warnings.filterwarnings("ignore")

def _save_user_changes(user, form):
    user.title = form.title.data
    user.firstName = form.firstName.data
    user.middleName = form.middleName.data
    user.lastName = form.lastName.data
    user.preferredName = form.preferredName.data
    user.dob = form.dob.data
    user.disabilities = form.disabilities.data
    user.mobile = form.mobile.data
    user.primary_postal_addr_building_name = form.primary_postal_addr_building_name.data
    user.primary_postal_addr_unit_details = form.primary_postal_addr_unit_details.data
    user.primary_postal_addr_street_no_name = form.primary_postal_addr_street_no_name.data
    user.primary_postal_addr_po_box = form.primary_postal_addr_po_box.data
    user.primary_postal_addr_city_suburb = form.primary_postal_addr_city_suburb.data
    user.primary_postal_addr_state = form.primary_postal_addr_state.data
    user.primary_postal_addr_post_code = form.primary_postal_addr_post_code.data
    user.primary_postal_addr_country = form.primary_postal_addr_country.data
    user.primary_street_addr_building_name = form.primary_street_addr_building_name.data
    user.primary_street_addr_unit_details = form.primary_street_addr_unit_details.data
    user.primary_street_addr_street_no_name = form.primary_street_addr_street_no_name.data
    user.primary_street_addr_po_box = form.primary_street_addr_po_box.data
    user.primary_street_addr_city_suburb = form.primary_street_addr_city_suburb.data
    user.primary_street_addr_state = form.primary_street_addr_state.data
    user.primary_street_addr_post_code = form.primary_street_addr_post_code.data
    user.primary_street_addr_country = form.primary_street_addr_country.data
    user.RACGP_no = form.RACGP_no.data
    user.gender = form.gender.data
    user.country_of_birth = form.country_of_birth.data
    user.city_of_birth = form.city_of_birth.data
    user.country_of_citizenship = form.country_of_citizenship.data
    user.citizenship_status = form.citizenship_status.data
    user.aboriginal_origin = True if form.aboriginal_origin.data == '1' else False
    user.torres_strait_islander_origin = True if form.torres_strait_islander_origin.data == '1' else False
    user.employment_status = form.employment_status.data
    user.lang_identifier = form.lang_identifier.data
    user.english_proficiency = form.english_proficiency.data
    user.english_assistance = True if form.english_assistance.data == '1' else False
    user.attending_other_school = True if form.attending_other_school.data == '1' else False
    user.school_completed_level = form.school_completed_level.data
    user.survey_contact_status = form.survey_contact_status.data
    user.contact_name = form.contact_name.data
    user.relationship = form.relationship.data
    user.contact_number = form.contact_number.data

    if form.comments:
        user.comments = form.comments.data
    
    if hasattr(form,'usi_exempt'):
        user.usi_exempt = form.usi_exempt.data
    
    if form.comments:
        user.comments = form.comments.data

    #urole field is only present in edit_user.
    #it is dropped in edit_profile so that oneself can't change their own role.
    if form.urole:
        user.urole = form.urole.data
    
    if form.contact_active:
        user.contact_active = True if form.contact_active.data == '1' else False
    return

def _update_fields_for_radio(obj,radios):
    """
        A pseduo copy appraoch
        makes `obj` transient to revoke from session
        this will enable us to make changes to `obj` without any conflict in commit.

        Replaces DB model `True` value to `1` and `False` to `0` which required to set correct radio.

    Args:
        obj (Sqlalchemy ORM Object): Object having radio buttons
        radios (list): radio buttons attribute names in a list.

    Returns:
        obj (Sqlalchemy ORM Object): Modified Sqlalchemy ORM Object. 
    """
    make_transient(obj)
    for radio in radios:
        setattr(obj,radio,'1') if getattr(obj,radio) else setattr(obj,radio,'0')
    return obj

def role_required(*roles):
    def wrapper(fn):
        @wraps(fn)
        def decorated_view(*args, **kwargs):
            if not current_user.is_authenticated:
                return app.login_manager.unauthorized()
            # urole = app.login_manager.reload_user().get_urole()
            urole = current_user.get_urole()  
            if len(roles)>3:
                raise Exception('Only three roles are allowed.')

            if len(roles)==1:
                if urole !=roles[0] and roles[0] != "ANY":
                    return app.login_manager.unauthorized()
            else:
                if urole  != roles[0] and  urole != roles[1] and urole != roles[2]:
                    return app.login_manager.unauthorized()

            return fn(*args, **kwargs)

        return decorated_view

    return wrapper


def preview_text(text):
    return (text[:app.config['PREVIEW_LENGTH'] - 3] + '...') if len(text) > app.config['PREVIEW_LENGTH'] else text


@app.before_request
def before_request():
    if current_user.is_authenticated:
        current_user.last_seen = datetime.utcnow()
        db.session.commit()
        g.search_form = SearchForm()


@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
@app.route('/courses', methods=['GET', 'POST'])
@login_required
def index():
    page = request.args.get('page', 1, type=int)
    courses = Course.query.filter(Course.date >= datetime.today()).filter(Course.inhouse == False).order_by(Course.date.asc()).paginate(
        page, app.config['POSTS_PER_PAGE'], False)
    next_url = url_for('index', page=courses.next_num) \
        if courses.has_next else None
    prev_url = url_for('index', page=courses.prev_num) \
        if courses.has_prev else None
    message = "these are the upcoming Pulsestart courses."
    return render_template("courses.html", title='Courses', message=message, courses=courses.items,
                           next_url=next_url, prev_url=prev_url)


@app.route('/', methods=['GET', 'POST'])
@app.route('/my_courses', methods=['GET', 'POST'])
@login_required
def myCourses():
    page = request.args.get('page', 1, type=int)
    myCourses = CourseAttendees.query.filter_by(user_id=current_user.id).all()
    enrolled = []
    for course in myCourses:
        enrolled.append(course.course_id)
    courses = Course.query.filter(Course.id.in_(enrolled)).order_by(Course.date.asc()).paginate(
        page, app.config['POSTS_PER_PAGE'], False)
    next_url = url_for('myCourses', page=courses.next_num) \
        if courses.has_next else None
    prev_url = url_for('myCourses', page=courses.prev_num) \
        if courses.has_prev else None
    message = "these are your enrolled courses."
    return render_template("courses.html", title='My Enrolled Courses', message = message, 
        courses=courses.items, next_url=next_url, prev_url=prev_url)

@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid email or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('index')
        return redirect(next_page)
    return render_template('login.html', title='Sign In', form=form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))


def parse_str_bool(value):
    return {
        '1': True,
        '0': False
    }[value]


def parse_form_fields(form, *keys):
    result = {}
    for key in keys:
        field = getattr(form, key)
        if isinstance(field, RadioField):
            #skipping radiofield boolean conversion if radio has more than two values
            if int(getattr(field, 'data')) <2:          
                data = parse_str_bool(getattr(field, 'data'))
            else:
                data = getattr(field, 'data')
        else:
            data = getattr(field, 'data')
        result.update({key: data})
        if key == 'usi_exempt':
            field = getattr(form, key)
            val = getattr(field, 'data')
            if val:
                result['USI'] = ''
            else:
                pass
    return result


@app.route('/register', methods=['GET', 'POST'])
def register():

    forms = {

        1:PersonalDetailsForm(),
        2:AddressDetailsForm(),
        3:EducationDetailsForm(),
        4:EmergenceyContactForm(),
        5:UserCredentialForm()
    }

    if current_user.is_authenticated:
        return redirect(url_for('index'))

    page = request.form.get('page',1,type=int)   
    form = forms.get(page)

    if isinstance(form,PersonalDetailsForm):
        del form.usi_exempt

    registration_cache_selector = f'register_cache_{page}'

    #functionality for back btn
    #get current page and selects prev form with data from session.
    if request.method == 'POST' and request.form.get('back') =='1':
        prev_page = page - 1
        prev_registration_cache_selector = f'register_cache_{prev_page}'
        
        form = forms.get(prev_page)
        
        if isinstance(form,PersonalDetailsForm):
            del form.usi_exempt

        prev_form_data = session[prev_registration_cache_selector]
        # print(prev_form_data)
        for key,value in prev_form_data.items():
            field = getattr(form,key)
    
            if  key == "dob":
                value = datetime.strptime(value,"%a, %d %b %Y %H:%M:%S GMT")
        
            setattr(field,'data',value)

        return render_template('register.html', title='Register',form=form)

    #when form is submitted i.e. next page is requested
    if form.validate_on_submit():
        form_data = form.data
        current_page  = int(form_data['page'])

        del form_data['csrf_token']

        session[registration_cache_selector] = form_data

        if current_page < len(forms):
            next_page = current_page + 1
        else:
            #No  next pages avaiable saving form..
            complete_form = RegistrationForm()
            del complete_form.usi_exempt

            for i in range(len(forms)):
                cache_selector = f'register_cache_{i+1}'
                cache = session[cache_selector]
                del cache['page']
                for key,value in cache.items():
                    field = getattr(complete_form,key)
                    if  key == "dob":
                        value = datetime.strptime(value,"%a, %d %b %Y %H:%M:%S GMT")
                    setattr(field,'data',value)
                #removing already  processed session cache.
                del session[cache_selector]
            
            user = User(
                **parse_form_fields(
                    complete_form,
                    'title', 'firstName', 'middleName', 'lastName', 'preferredName', 'dob',
                    'mobile', 'email','disabilities',
                    'primary_postal_addr_building_name', 'primary_postal_addr_unit_details',
                    'primary_postal_addr_street_no_name', 'primary_postal_addr_po_box', 'primary_postal_addr_city_suburb',
                    'primary_postal_addr_state', 'primary_postal_addr_post_code', 'primary_postal_addr_country',

                    'primary_street_addr_building_name', 'primary_street_addr_unit_details',
                    'primary_street_addr_street_no_name', 'primary_street_addr_po_box', 'primary_street_addr_city_suburb',
                    'primary_street_addr_state', 'primary_street_addr_post_code', 'primary_street_addr_country',

                    'RACGP_no',

                    'gender', 'country_of_birth', 'city_of_birth', 'country_of_citizenship', 'citizenship_status',
                    'aboriginal_origin', 'torres_strait_islander_origin', 'employment_status',
                    'lang_identifier', 'english_proficiency', 'english_assistance', 'attending_other_school',
                    'school_completed_level', 'survey_contact_status',

                    'contact_name', 'relationship', 'contact_number','USI'
                )

            )
            user.set_password(complete_form.password.data)
            db.session.add(user)
            db.session.commit()
            flash('Congratulations, you are now a registered user! Sign in below.')
            return redirect(url_for('login'))
                            
        form = forms.get(next_page)
        form.page.data = next_page

    return render_template('register.html', title='Register',form=form)

        
        

        
        # user = User(
        #     **parse_form_fields(
        #         form,
        #         'title', 'firstName', 'middleName', 'lastName', 'preferredName', 'dob',
        #         'mobile', 'email','disabilities',
        #         'primary_postal_addr_building_name', 'primary_postal_addr_unit_details',
        #         'primary_postal_addr_street_no_name', 'primary_postal_addr_po_box', 'primary_postal_addr_city_suburb',
        #         'primary_postal_addr_state', 'primary_postal_addr_post_code', 'primary_postal_addr_country',

        #         'primary_street_addr_building_name', 'primary_street_addr_unit_details',
        #         'primary_street_addr_street_no_name', 'primary_street_addr_po_box', 'primary_street_addr_city_suburb',
        #         'primary_street_addr_state', 'primary_street_addr_post_code', 'primary_street_addr_country',

        #         'RACGP_no',

        #         'gender', 'country_of_birth', 'city_of_birth', 'country_of_citizenship', 'citizenship_status',
        #         'aboriginal_origin', 'torres_strait_islander_origin', 'employment_status',
        #         'lang_identifier', 'english_proficiency', 'english_assistance', 'attending_other_school',
        #         'school_completed_level', 'survey_contact_status',

        #         'contact_name', 'relationship', 'contact_number','agent','USI'
        #     )
        # )
        # user.set_password(form.password.data)
        # db.session.add(user)
        # db.session.commit()
        # flash('Congratulations, you are now a registered user! Sign in below.')
        # return redirect(url_for('login'))



@app.route('/user/<user_uuid>')
@login_required
def user(user_uuid):
    user = User.query.filter_by(user_uuid=user_uuid).first_or_404()
    if user == current_user:
        page = request.args.get('page', 1, type=int)
        posts = user.posts.order_by(Post.timestamp.desc()).paginate(
            page, app.config['POSTS_PER_PAGE'], False)
        next_url = url_for('user', user_uuid=user.user_uuid, page=posts.next_num) \
            if posts.has_next else None
        prev_url = url_for('user', user_uuid=user.user_uuid, page=posts.prev_num) \
            if posts.has_prev else None
        form = EmptyForm()
        return render_template('user.html', user=user, posts=posts.items,
                               next_url=next_url, prev_url=prev_url, form=form)
    else:
        return render_template('404.html', title='404'), 404


@app.route('/user/edit_profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    #copying current user id for later query
    user_id = current_user.id

    radios = [
                'aboriginal_origin',
                'torres_strait_islander_origin',
                'english_assistance',
                'attending_other_school',
            ]
    form_user =  _update_fields_for_radio(current_user,radios)
    form = EditUserForm(formdata=request.form, obj=form_user)

    #oneself shouldn't be allowed to change its own role so dropping the urole field.
    del form.urole
    #dropping other unwanted fields.
    del form.contact_active
    del form.comments
    #as current user is revoked from session
    #quering a new session persistent user.
    persistent_user = User.query.filter_by(id=user_id).first()
    if form.validate_on_submit():
        _save_user_changes(persistent_user, form)
        db.session.commit()
        flash('Your changes have been saved.')
        return redirect(url_for('edit_profile'))
    return render_template('register.html', title='Edit Profile', form=form)


@app.route('/explore')
@login_required
@role_required("Admin")
def explore():
    page = request.args.get('page', 1, type=int)
    posts = Post.query.order_by(Post.timestamp.desc()).paginate(
        page, app.config['POSTS_PER_PAGE'], False)
    next_url = url_for('explore', page=posts.next_num) \
        if posts.has_next else None
    prev_url = url_for('explore', page=posts.prev_num) \
        if posts.has_prev else None
    return render_template("index.html", title='Explore', posts=posts.items,
                           next_url=next_url, prev_url=prev_url)


@app.route('/course/<course_id>', methods=['GET', 'POST'])
@login_required
def course(course_id):
    course = Course.query.filter_by(id=course_id).first_or_404()
    if course.inhouse == True:
        return render_template('404.html', title='404'), 404
    if current_user.urole != "Admin" and course.date.date() < datetime.today().date():
            return render_template('404.html', title='404'), 404
    attendeeList = CourseAttendees.query.filter_by(course_id=course_id).all()
    is_enrolled = CourseAttendees.query.filter_by(course_id=course_id,user_id=current_user.id).first()
    enrolled = True if is_enrolled else False
    courseFull = False
    course_quiz = Quiz.query.filter_by(course_id=course.id).first()
    if course_quiz:
        quizComplete = QuizResult.query.filter_by(quiz_id=course_quiz.id, user_id=current_user.id).first()
        isQuiz = True
    else:
        quizComplete = None
        isQuiz = False
    if len(attendeeList) >= course.maxAttendees: courseFull = True

    if course.close_course_before_start:
        close_course = True if abs((datetime.now() - course.date).days) <= course.close_course_before_start else False
    else:
        close_course = False

    form = ContactForm()
    form.name.data = current_user.firstName + " " + current_user.lastName
    form.email.data = current_user.email
    form.subject.data = "Enquiry: " + course.title
    if request.method == 'POST':
        send_course_enquiry(current_user, request.values)
        flash('Your enquiry has been sent!')
        return render_template('course.html', course=course, form=form,
                         courseFull=courseFull, close_course=close_course,enrolled=enrolled,
                         quizComplete=quizComplete,isQuiz=isQuiz)
    else:
        return render_template('course.html', course=course, form=form,
                         courseFull=courseFull, close_course=close_course,enrolled=enrolled,
                         quizComplete=quizComplete,isQuiz=isQuiz)

@app.route('/course/<int:course_id>/quiz',methods=['GET','POST'])
@login_required
def course_quiz(course_id):
    """
    #!!!This is a dummy JSON Object for HOW QUIZ QUESTIONS SHOULD BE STRUCTURED!!!
    #!!!AS PER CURRENT IMPLEMENTATION  THIS STRUCTURE SHOULD BE STRICTLY FOLLOWED!!!
    questions = [
                {
                    "id": "q1",
                    "question": "What is the Capital of Syria?",
                    "choices": [['1', "Beirut"], ['2', "Damascus"], ['3', "Baghdad"]],
                    "correct": ['2', 'Damascus']
                },
                {
                    "id": "q2",
                    "question": "What is the Capital of Nepal?",
                    "choices": [['1', "Katmandu"], ['2', "New Delhi"], ['3', "Moscow"]],
                    "correct": ['1', 'Kathmandu']
                },
                                {
                    "id": "q3",
                    "question": "What is the Capital of China?",
                    "choices": [['1', "Bejing"], ['2', "Xinjang"], ['3', "Hirosima"]],
                    "correct": ['1', 'Bejing']
                },         
                {
                    "id": "q4",
                    "question": "What is the Capital of USA?",
                    "choices": [['1', "New York"], ['2', "Alabama"], ['3', "DC"]],
                    "correct": ['3', 'DC']
                },
            ]
    """
    #only allowing users  to take quiz  if user has enrolled to the course
    CourseAttendees.query.filter_by(course_id=course_id,user_id=current_user.id).first_or_404()
    page = request.form.get('page',0,type=int)

    session_selector = f'quiz_{page}_cache'
    quiz = Quiz.query.filter_by(course_id=course_id).first_or_404()
    quiz_templates = quiz.quiz_templates
    selected_template = quiz_templates[page]

    title = selected_template.title
    url = selected_template.video_url
    if selected_template.image:
        image_path = selected_template.image
    else:
        image_path = ''
    
    #getting which question to render accroding to current page
    questions = selected_template.questions

    #current question form set with current page
    form = DynamicQuizForm(questions,page)

    #if it's a single page quiz
    if len(quiz_templates) == 1:
        form.submit.label.text = 'Submit Quiz'


    if form.validate_on_submit():
        answers = []
        total = len(questions)
        score = 0
        # preparing answer json object for user 
        for question in questions:
            form_field = getattr(form,question['id'])
            field_data = getattr(form_field,'data')
            #adding user answer to existing questions.
            question['answer'] = field_data
            answers.append(question)


        #saving current ansers to session
        #ansers will be calculated at last
        session[session_selector] = answers

        #checking if next page exits for the quiz
        if page < len(quiz_templates)-1:
            next_page = page +1
        else:
            #no next page available. So this will be the last page.
            #quiz calculatoin will be done over here
            score = 0
            total = 0
            all_answers = []
            for i in range(len(quiz_templates)):
                quiz_cache_selector = f'quiz_{i}_cache'
                quiz_caches = session[quiz_cache_selector]
                for answer in quiz_caches:
                    all_answers.append(answer)
                    #calc total questions/answers
                    total += 1
                    #calc score
                    if answer['answer'] == answer['correct'][0]:
                        score += 1
                
                #removing quiz cache session
                del session[quiz_cache_selector]

                        
            quiz_score = f'{score}/{total}'

            quiz_result = QuizResult(
                quiz_id=quiz.id,answers=all_answers,
                result=quiz_score,user_id=current_user.id
            )
            db.session.add(quiz_result)
            db.session.commit()

            flash('Congratulations! You have scored '+quiz_score)
            if Course.query.filter(Course.id==course_id).first().inhouse == 0:
                return redirect(url_for('course',course_id=course_id))
            else:
                course_inhouse_uuid = Course.query.filter(Course.id==course_id).first().inhouse_uuid
                return redirect(url_for('inhouse_course',course_inhouse_uuid=course_inhouse_uuid))


        #is this last page to render ?
        last_page = True if (next_page == len(quiz_templates) -1) else False
        
        #fetching next questions from tempalte
        next_selected_template = quiz_templates[next_page]
        next_questions = next_selected_template.questions
        title = next_selected_template.title
        url = next_selected_template.video_url
        if next_selected_template.image:
            image_path = next_selected_template.image
        else:
            image_path = ''



        

        #****setting current page from this method didn't worked
        #could be a wtf-form bug******
        next_form = DynamicQuizForm(next_questions,next_page)
        #updating current page
        next_form.page.data = str(next_page)

        #changing last page next btn text to submit quiz.
        if last_page:
            next_form.submit.label.text = 'Submit Quiz'
    
        return render_template('quiz.html',form=next_form,title=title,video_url=url,image_path=image_path)

    return render_template('quiz.html',form=form,title=title,video_url=url,image_path=image_path)
    
@app.route('/admin/create_quiz',methods=['GET','POST'])
@login_required
@role_required('Admin')
def create_quiz():

    if request.method == 'GET':
        return render_template('create_quiz.html')

    
    questions = request.form.getlist('question')
    correct_choices = request.form.getlist('correct')

    choices_1 = request.form.getlist('choice_1')
    choices_2 = request.form.getlist('choice_2')
    choices_3 = request.form.getlist('choice_3')

    image_file = request.files['image']
    

    file_ext = os.path.splitext(image_file.filename)[1]
    uploaded_image_name = str(uuid4().hex) + file_ext
    upload_folder_path = os.path.join(os.path.join(app.root_path),'static','images',uploaded_image_name)
  

    title = request.form.get('title')
    video_url  = request.form.get('video_url')

    def encode_quiz_template():
        """
        {
            "id": "q1",
            "question": "What is the Capital of Syria?",
            "choices": [['1', "Beirut"], ['2', "Damascus"], ['3', "Baghdad"]],
            "correct": ['2', 'Damascus']
        }
        """
        
        q_count = 1
        encoded = []
        for index,question in enumerate(questions):
            
            choice_string_map = {'1':choices_1[index],'2':choices_2[index],'3':choices_3[index]}
            q_no = 'q'+str(q_count)
            choices =  [
                            [ '1',choices_1[index] ],
                            [ '2',choices_2[index] ],
                            [ '3',choices_3[index] ],
                        ]
            correct  = [correct_choices[index],choice_string_map.get(correct_choices[index])]

            encoded.append({
                'id':q_no,
                'question':question,
                'choices':choices,
                'correct':correct
            })
            q_count += 1
        
        return encoded
    
    processed_json = encode_quiz_template()
    quiz_template = QuizTemplate(video_url=video_url,title=title,questions=processed_json,image=uploaded_image_name)
    db.session.add(quiz_template)
    db.session.commit()
    image_file.save(upload_folder_path)
    flash('Quiz Template Saved!')
    return redirect(url_for('create_quiz'))

@app.route('/inhouse-course/<course_inhouse_uuid>', methods=['GET', 'POST'])
@login_required
def inhouse_course(course_inhouse_uuid):
    course = Course.query.filter_by(inhouse_uuid=course_inhouse_uuid).first_or_404()
    if course.inhouse == False:
        return render_template('404.html', title='404'), 404
    if current_user.urole != "Admin" and course.date.date() < datetime.today().date():
            return render_template('404.html', title='404'), 404
    attendeeList = CourseAttendees.query.filter_by(course_id=course.id).all()
    is_enrolled = CourseAttendees.query.filter_by(course_id=course.id,user_id=current_user.id).first()
    enrolled = True if is_enrolled else False
    courseFull = False
    course_quiz = Quiz.query.filter_by(course_id=course.id).first()
    if course_quiz:
        quizComplete = QuizResult.query.filter_by(quiz_id=course_quiz.id, user_id=current_user.id).first()
        isQuiz = True
    else:
        quizComplete = None
        isQuiz = False
    if len(attendeeList) >= course.maxAttendees: courseFull = True

    if course.close_course_before_start:
        close_course = True if abs((datetime.now() - course.date).days) <= course.close_course_before_start else False
    else:
        close_course = False

    form = ContactForm()
    form.name.data = current_user.firstName + " " + current_user.lastName
    form.email.data = current_user.email
    form.subject.data = "Enquiry: " + course.title
    if request.method == 'POST':
        send_course_enquiry(current_user, request.values)
        flash('Your enquiry has been sent!')
        return render_template('course.html', course=course, form=form,
                         courseFull=courseFull, close_course=close_course,enrolled=enrolled,
                         quizComplete=quizComplete,isQuiz=isQuiz)
    else:
        return render_template('course.html', course=course, form=form,
                         courseFull=courseFull, close_course=close_course,enrolled=enrolled,
                         quizComplete=quizComplete,isQuiz=isQuiz)

@app.route('/course/admin/edit_course/<course_id>', methods=['GET', 'POST'])
@login_required
@role_required("Admin")
def edit_course(course_id):
    course = Course.query.filter_by(id=course_id).first_or_404()
    quiz_templates = QuizTemplate.query.all()
    quiz_template_choices = [[str(i.id),i.title+' '+str(i.id)] for i in quiz_templates]
    selected_course_quiz =  Quiz.query.filter(Quiz.course_id==course_id).first()
    already_selected = [str(i.id) for i in selected_course_quiz.quiz_templates]
    instructors = User.query.filter(User.urole=="Instructor").all()
    instructor_choices = [[str(i.id),i.firstName+' '+i.lastName] for i in instructors]
    form = CourseForm(formdata=request.form, obj=course)
    form.instructor.choices = instructor_choices
    form.quiz_template.choices = quiz_template_choices
    if form.validate_on_submit():
        course.title = form.title.data
        course.instructor = form.instructor.data
        course.date = form.date.data
        course.startTime = form.startTime.data
        course.endTime = form.endTime.data
        course.venue = form.venue.data
        course.cost = form.cost.data
        course.maxAttendees = form.maxAttendees.data
        course.description = form.description.data
        course.descriptionPreview = preview_text(form.description.data)
        course.course_code = form.course_code.data
        course.close_course_before_start = form.close_course_before_start.data

        incomming_quiz_templates = set(form.quiz_template.data)
        #only if quiz templates are selected in edit
        if incomming_quiz_templates:
            already_selected_quiz_templates = set(already_selected)

            to_add = incomming_quiz_templates - already_selected_quiz_templates
            to_remove = already_selected_quiz_templates - incomming_quiz_templates

            #adding new templates
            if to_add:
                for template in to_add:
                    new_template = QuizTemplate.query.filter_by(id=template).first()
                    selected_course_quiz.quiz_templates.append(new_template)

            #removing diselected templates  
            if to_remove:
                for template in to_remove:
                    old_template = QuizTemplate.query.filter_by(id=template).first()
                    selected_course_quiz.quiz_templates.remove(old_template)

        db.session.add(selected_course_quiz)
        db.session.commit()
        flash('Your changes have been saved.')
        return redirect(url_for('admin_courses'))
    else:
        if selected_course_quiz:
            form.quiz_template.data = already_selected

    return render_template('editCourse.html', title='Edit Course', form=form)


@app.route('/course/admin/<course_id>', methods=['GET', 'POST'])
@login_required
@role_required("Admin","Instructor")
def courseAdmin(course_id):
    course = Course.query.filter_by(id=course_id).first_or_404()
    attendeeList = CourseAttendees.query.filter_by(course_id=course_id).all()
    quiz_id = Quiz.query.filter_by(course_id=course.id).first().id
    attendees = []
    quizResults = []
    for attendee in attendeeList:
        attendees.append(User.query.filter_by(id=attendee.user_id).first())
        if quiz_id:
            quizResult = QuizResult.query.filter_by(quiz_id=quiz_id, user_id=attendee.user_id).first()
            if quizResult:
                quizResults.append(quizResult.result)
            else:
                quizResults.append("Not complete")
    else:
        return render_template('course.html', course=course, isAdminView=True, form=EmptyForm(), 
            attendees=attendees, quizResults=quizResults)


@app.route('/enrol', methods=['POST'])
@login_required
def enrol():
    course = Course.query.filter_by(id=request.values.get('course_id')).first_or_404()
    attendeeList = CourseAttendees.query.filter_by(course_id=request.values.get('course_id')).all()
    if len(attendeeList) >= course.maxAttendees:
        flash('This course is full, please select another date.')
        return render_template("enrol.html", title='Course Enrolment')
    for attendee in attendeeList:
        if attendee.user_id == current_user.id:
            flash('You are already enrolled in this course.')
            return render_template("enrol.html", title='Course Enrolment')
    # If payment token is valid
    courseAttendee = CourseAttendees(course_id=request.values.get('course_id'), user_id=current_user.id)
    db.session.add(courseAttendee)
    db.session.commit()
    send_enrol_confirmation(current_user, course)
    flash('Your enrolment is complete! Please check your email for details.')
    return render_template("enrol.html", title='Course Enrolment')


@app.route('/course/admin/create_course', methods=['GET', 'POST'])
@login_required
@role_required("Admin")
def createCourse():

    templates = QuizTemplate.query.all()
    choices = [[str(i.id),i.title+' '+str(i.id)] for i in templates]
    instructors = User.query.filter(User.urole=="Instructor").all()
    instructor_choices = [[str(i.id),i.firstName+' '+i.lastName] for i in instructors]
    form = CourseForm()
    form.quiz_template.choices = choices
    form.instructor.choices = instructor_choices
    if form.validate_on_submit():
        course = Course(title=form.title.data, date=form.date.data, instructor=form.instructor.data,
                        startTime=form.startTime.data, endTime=form.endTime.data,
                        venue=form.venue.data, cost=form.cost.data, course_code=form.course_code.data,
                        creator=current_user.id, maxAttendees=form.maxAttendees.data, inhouse = form.inhouse.data,
                        description=form.description.data, descriptionPreview=preview_text(form.description.data),
                        close_course_before_start=form.close_course_before_start.data)
        
        #creating quiz only if templates are selected.
        if form.quiz_template.data:
            #creating quiz
            quiz = Quiz()
            #assigning quiz template to quiz
            quiz.quiz_templates = [QuizTemplate.query.filter_by(id=i).first() for i in form.quiz_template.data]
            #assigning course to quiz.
            quiz.course =course
            db.session.add(quiz)

        db.session.add(course)
        db.session.commit()

        if form.inhouse.data == True:
            send_admin_inhouse_course_alert(course)
        flash('Your course is now posted!')
        return redirect(url_for('index'))
    return render_template("createCourse.html", title='Create Course', form=form)


@app.route('/course/admin/create_template', methods=['GET', 'POST'])
@login_required
@role_required("Admin")
def createTemplate():
    form = TemplateForm()
    #only listing not assigned templates
    templates = QuizTemplate.query.all()
    choices = [[str(i.id),i.title+' '+str(i.id)] for i in templates]
    form.quiz_template.choices = choices

    if form.validate_on_submit():
        template = Template(title=form.title.data,
                            venue=form.venue.data, cost=form.cost.data,
                            creator=current_user.id, maxAttendees=form.maxAttendees.data,
                            description=form.description.data, descriptionPreview=preview_text(form.description.data))

        template.quiz_templates = [QuizTemplate.query.filter_by(id=i).first() for i in form.quiz_template.data]
        db.session.add(template)
        db.session.commit()
        flash('Your template is now posted!')
        return redirect(url_for('index'))
    

    return render_template("createTemplate.html", title='Create Template', form=form)


@app.route('/course/admin/select_template', methods=['GET', 'POST'])
@login_required
@role_required("Admin")
def selectTemplate():
    templates = Template.query.order_by(Template.title.asc())

    quiz_templates = QuizTemplate.query.all()
    choices = [[str(i.id),i.title+' '+str(i.id)] for i in quiz_templates]
    instructors = User.query.filter(User.urole=="Instructor").all()
    instructor_choices = [[str(i.id),i.firstName+' '+i.lastName] for i in instructors]

    if request.method == 'POST':
        form = CourseForm()
        form.quiz_template.choices=choices
        form.instructor.choices = instructor_choices
        if request.values.get('templateName'):
            # If templateName, the user has chosen a template
            # Render course page pre filled with the template data
            chosen = [template for template in templates if template.id == int(request.values.get('templateName'))][0]
            form.title.data = chosen.title
            form.venue.data = chosen.venue
            form.cost.data = chosen.cost
            form.maxAttendees.data = chosen.maxAttendees
            form.description.data = chosen.description
            form.quiz_template.data = [str(i.id) for i in chosen.quiz_templates]
            return render_template("createCourse.html", title='Create Course', form=form)
        else:
            # If no templateName, the user has submitted a new course to be committed to db
            if form.validate_on_submit():
                course = Course(title=form.title.data, date=form.date.data, instructor=form.instructor.data,
                        startTime=form.startTime.data, endTime=form.endTime.data,
                        venue=form.venue.data, cost=form.cost.data, course_code=form.course_code.data,
                        creator=current_user.id, maxAttendees=form.maxAttendees.data, inhouse = form.inhouse.data,
                        description=form.description.data, descriptionPreview=preview_text(form.description.data))
                
                #only creating course when there is no template
                if form.quiz_template.data:
                    #creating quiz
                    quiz = Quiz()
                    #assigning quiz template to quiz
                    quiz.quiz_templates = [QuizTemplate.query.filter_by(id=i).first() for i in form.quiz_template.data]
                    #assigning course to quiz.
                    quiz.course = course
                    db.session.add(quiz)

                db.session.add(course)
                db.session.commit()

                if form.inhouse.data == True:
                    send_admin_inhouse_course_alert(course)
                flash('Your course is now posted!')
                return redirect(url_for('index'))
            else:
                return render_template("createCourse.html", title='Create Course', form=form)
    else:
        return render_template("selectTemplate.html", title='Select Template', templates=templates)


@app.route('/user/reset_password_request', methods=['GET', 'POST'])
def reset_password_request():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = ResetPasswordRequestForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user:
            send_password_reset_email(user)
        flash('Check your email for the instructions to reset your password')
        return redirect(url_for('login'))
    return render_template('reset_password_request.html',
                           title='Reset Password', form=form)


@app.route('/user/reset_password/<token>', methods=['GET', 'POST'])
def reset_password(token):
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    user = User.verify_reset_password_token(token)
    if not user:
        return redirect(url_for('index'))
    form = ResetPasswordForm()
    if form.validate_on_submit():
        user.set_password(form.password.data)
        db.session.commit()
        flash('Your password has been reset.')
        return redirect(url_for('login'))
    return render_template('reset_password.html', form=form)


@app.route('/user/create_password/<token>', methods=['GET', 'POST'])
def create_password(token):
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    user = User.verify_reset_password_token(token)
    if not user:
        return redirect(url_for('index'))
    form = CreatePasswordForm()
    if form.validate_on_submit():
        user.set_password(form.password.data)
        db.session.commit()
        flash('Your password has been set.')
        return redirect(url_for('login'))
    return render_template('create_password.html', form=form)

@app.route('/user/change_password',methods=['GET','POST'])
@login_required
def change_password():
    if not current_user.is_authenticated:
        return redirect(url_for('index'))
    g.current_user = current_user
    form = ChangePasswordForm()
    if form.validate_on_submit():
        g.current_user.set_password(form.password.data)
        db.session.commit()
        flash('Your password has been changed.')
        return redirect(url_for('user',user_uuid=g.current_user.user_uuid))
    return render_template('change_password.html',form=form)

@app.route('/search')
@login_required
def search():
    if not g.search_form.validate():
        return redirect(url_for('explore'))
    page = request.args.get('page', 1, type=int)
    posts, total = Post.search(g.search_form.q.data, page,
                               app.config['POSTS_PER_PAGE'])
    next_url = url_for('search', q=g.search_form.q.data, page=page + 1) \
        if total > page * app.config['POSTS_PER_PAGE'] else None
    prev_url = url_for('search', q=g.search_form.q.data, page=page - 1) \
        if page > 1 else None
    return render_template('search.html', title=('Search'), posts=posts,
                           next_url=next_url, prev_url=prev_url)


@app.route('/send_message/<recipient>', methods=['GET', 'POST'])
@login_required
def send_message(recipient):
    user = User.query.filter_by(id=recipient).first_or_404()
    form = MessageForm()
    if form.validate_on_submit():
        msg = Message(author=current_user, recipient=user,
                      body=form.message.data)
        db.session.add(msg)
        user.add_notification('unread_message_count', user.new_messages())
        db.session.commit()
        flash('Your message has been sent.')
        return redirect(url_for('user', id=recipient))
    return render_template('send_message.html', title=('Send Message'),
                           form=form, recipient=recipient)


@app.route('/messages')
@login_required
def messages():
    current_user.last_message_read_time = datetime.utcnow()
    current_user.add_notification('unread_message_count', 0)
    db.session.commit()
    page = request.args.get('page', 1, type=int)
    messages = current_user.messages_received.order_by(
        Message.timestamp.desc()).paginate(
        page, app.config['POSTS_PER_PAGE'], False)
    next_url = url_for('main.messages', page=messages.next_num) \
        if messages.has_next else None
    prev_url = url_for('main.messages', page=messages.prev_num) \
        if messages.has_prev else None
    return render_template('messages.html', messages=messages.items,
                           next_url=next_url, prev_url=prev_url)


@app.route('/notifications')
@login_required
def notifications():
    since = request.args.get('since', 0.0, type=float)
    notifications = current_user.notifications.filter(
        Notification.timestamp > since).order_by(Notification.timestamp.asc())
    return jsonify([{
        'name': n.name,
        'data': n.get_data(),
        'timestamp': n.timestamp
    } for n in notifications])


@app.route('/course/admin/courses')
@login_required
@role_required("Admin")
def admin_courses():
    page = request.args.get('page', 1, type=int)
    filter_course = request.args.get('filter',type=str)
    filter_time = request.args.get('time',type=str)
    search_course = request.args.get('search_course',type=str)
    search_venue = request.args.get('search_venue',type=str)
    search_month = request.args.get('search_month',type=str)
    search_date = request.args.get('search_date',type=str)
    
    #preparing filter query conditions for time 
    if filter_time == 'past':
        condition1 = Course.date <= datetime.now()
    elif filter_time == 'future':
        condition1 = Course.date >= datetime.now()
    else:
        condition1 =  sql.true()
    
    #preparing filter query conditions for course type
    if filter_course == 'inhouse':
        condition2 = Course.inhouse == True
    else:
        condition2 = sql.true()

    #preparing filter query conditions for course title
    if search_course:
        condition3  = Course.title.ilike((f'%{search_course}%'))
    else:
        condition3 = sql.true()

    #preparing filter query conditions for course venue
    if search_venue:
        condition4 = Course.venue.ilike((f'%{search_venue}%'))
    else:
        condition4 = sql.true()
    
    #preparing filter query conditions for course available within a month
    if search_month:
        start_month = datetime.strptime(search_month,"%m/%Y")
        end_month = start_month + relativedelta(day=31)
        condition5 = and_(Course.date >= start_month, Course.date<=end_month)
    else:
        condition5 = sql.true()
    
    #preparing filter query conditions for course date
    if search_date:
        date = datetime.strptime(search_date,"%Y-%m-%d")
        condition6 = Course.date == date
    else:
        condition6 = sql.true()
    
    courses = Course.query.filter(condition1,condition2,
                                condition3,condition4,
                                condition5,condition6).\
                                order_by(Course.date.asc()).paginate(
                                    page,app.config['POSTS_PER_PAGE'],False
                                )
                                
    next_url = url_for('admin_courses', page=courses.next_num,
    filter=filter_course,time=filter_time,search_course=search_course,
            search_venue=search_venue,search_date=search_date,search_month=search_month) \
        if courses.has_next else None
    prev_url = url_for('admin_courses', page=courses.prev_num,
    filter=filter_course,time=filter_time,search_course=search_course,
    search_venue=search_venue,search_date=search_date,search_month=search_month) \
        if courses.has_prev else None
    return render_template("admin_courses.html", title='Courses', courses=courses.items,
                           next_url=next_url, prev_url=prev_url)


@app.route('/course/instructor/courses')
@login_required
@role_required("Instructor")
def instructor_courses():
    page = request.args.get('page', 1, type=int)
    filter_course = request.args.get('filter',type=str)
    filter_time = request.args.get('time',type=str)
    search_course = request.args.get('search_course',type=str)
    search_venue = request.args.get('search_venue',type=str)
    search_month = request.args.get('search_month',type=str)
    search_date = request.args.get('search_date',type=str)
    
    #preparing filter query conditions for time 
    if filter_time == 'past':
        condition1 = Course.date <= datetime.now()
    elif filter_time == 'future':
        condition1 = Course.date >= datetime.now()
    else:
        condition1 =  sql.true()
    
    #preparing filter query conditions for course type
    if filter_course == 'inhouse':
        condition2 = Course.inhouse == True
    else:
        condition2 = sql.true()

    #preparing filter query conditions for course title
    if search_course:
        condition3  = Course.title.ilike((f'%{search_course}%'))
    else:
        condition3 = sql.true()

    #preparing filter query conditions for course venue
    if search_venue:
        condition4 = Course.venue.ilike((f'%{search_venue}%'))
    else:
        condition4 = sql.true()
    
    #preparing filter query conditions for course available within a month
    if search_month:
        start_month = datetime.strptime(search_month,"%m/%Y")
        end_month = start_month + relativedelta(day=31)
        condition5 = and_(Course.date >= start_month, Course.date<=end_month)
    else:
        condition5 = sql.true()
    
    #preparing filter query conditions for course date
    if search_date:
        date = datetime.strptime(search_date,"%Y-%m-%d")
        condition6 = Course.date == date
    else:
        condition6 = sql.true()
    
    courses = Course.query.filter(condition1,condition2,
                                condition3,condition4,
                                condition5,condition6,Course.instructor==current_user.id).\
                                order_by(Course.date.asc()).paginate(
                                    page,app.config['POSTS_PER_PAGE'],False
                                )
                                
    next_url = url_for('instructor_courses', page=courses.next_num,
    filter=filter_course,time=filter_time,search_course=search_course,
            search_venue=search_venue,search_date=search_date,search_month=search_month) \
        if courses.has_next else None
    prev_url = url_for('instructor_courses', page=courses.prev_num,
    filter=filter_course,time=filter_time,search_course=search_course,
    search_venue=search_venue,search_date=search_date,search_month=search_month) \
        if courses.has_prev else None
    return render_template("instructor_courses.html", title='My Courses', courses=courses.items,
                           next_url=next_url, prev_url=prev_url)


@app.route('/user/admin/enrol', methods=['GET', 'POST'])
@login_required
@role_required("Admin")
def admin_user_enrol():
    forms = {

        1:PersonalDetailsForm(),
        2:AddressDetailsForm(),
        3:EducationDetailsForm(),
        4:EmergenceyContactForm(),
        5:UserRegistrationAdminOnlyForm()
    }


    page = request.form.get('page',1,type=int)   
    form = forms.get(page)

    registration_cache_selector = f'admin_register_cache_{page}'

    #functionality for back btn
    #get current page and selects prev form with data from session.
    if request.method == 'POST' and request.form.get('back') =='1':
        prev_page = page - 1
        prev_registration_cache_selector = f'admin_register_cache_{prev_page}'
        
        form = forms.get(prev_page)

    

        prev_form_data = session[prev_registration_cache_selector]
        # print(prev_form_data)
        for key,value in prev_form_data.items():
            field = getattr(form,key)
    
            if  key == "dob":
                value = datetime.strptime(value,"%a, %d %b %Y %H:%M:%S GMT")
        
            setattr(field,'data',value)

        return render_template('register.html', title='Register',form=form)

    #when form is submitted i.e. next page is requested
    if form.validate_on_submit():
        form_data = form.data
        current_page  = int(form_data['page'])

        del form_data['csrf_token']

        session[registration_cache_selector] = form_data

        if current_page < len(forms):
            next_page = current_page + 1
            form = forms.get(next_page)
            form.page.data = next_page
        else:
            #No  next pages avaiable saving form..
            complete_form = AdminCreateUserForm()

            for i in range(len(forms)):
                cache_selector = f'admin_register_cache_{i+1}'
                cache = session[cache_selector]
                del cache['page']

                if 'autocomplete_postal_address' in cache:
                    del cache['autocomplete_postal_address']
                    del cache['postal_street_number']

                for key,value in cache.items():
                    field = getattr(complete_form,key)
                    if  key == "dob":
                        value = datetime.strptime(value,"%a, %d %b %Y %H:%M:%S GMT")
                    setattr(field,'data',value)
                #removing already  processed session cache.
                del session[cache_selector]
            
            user = User(
                **parse_form_fields(
                    complete_form,
                    'title', 'firstName', 'middleName', 'lastName', 'preferredName', 'dob',
                    'mobile', 'email','disabilities',
                    'primary_postal_addr_building_name', 'primary_postal_addr_unit_details',
                    'primary_postal_addr_street_no_name', 'primary_postal_addr_po_box', 'primary_postal_addr_city_suburb',
                    'primary_postal_addr_state', 'primary_postal_addr_post_code', 'primary_postal_addr_country',

                    'primary_street_addr_building_name', 'primary_street_addr_unit_details',
                    'primary_street_addr_street_no_name', 'primary_street_addr_po_box', 'primary_street_addr_city_suburb',
                    'primary_street_addr_state', 'primary_street_addr_post_code', 'primary_street_addr_country',

                    'RACGP_no',

                    'gender', 'country_of_birth', 'city_of_birth', 'country_of_citizenship', 'citizenship_status',
                    'aboriginal_origin', 'torres_strait_islander_origin', 'employment_status',
                    'lang_identifier', 'english_proficiency', 'english_assistance', 'attending_other_school',
                    'school_completed_level', 'survey_contact_status',

                    'contact_name', 'relationship', 'contact_number','comments','contact_active','usi_exempt',

                    'USI'
                )
            )
            db.session.add(user)
            db.session.commit()
            send_password_create_email(user)
            flash('Congratulations, user has been created successfully. An email has been sent to create a password.')
            return redirect(url_for('users'))


    return render_template('register.html', title='Register',form=form)

    # form = AdminCreateUserForm()
    # if form.validate_on_submit():
    #     """user = User(firstName=form.firstName.data,
    #                 lastName=form.lastName.data,
    #                 mobile=form.mobile.data,
    #                 email=form.email.data,
    #                 USI=form.USI.data)"""
    #     user = User(
    #         **parse_form_fields(
    #             form,
    #            'title', 'firstName', 'middleName', 'lastName', 'preferredName', 'dob',
    #             'mobile', 'email','disabilities',
    #             'primary_postal_addr_building_name', 'primary_postal_addr_unit_details',
    #             'primary_postal_addr_street_no_name', 'primary_postal_addr_po_box', 'primary_postal_addr_city_suburb',
    #             'primary_postal_addr_state', 'primary_postal_addr_post_code', 'primary_postal_addr_country',

    #             'primary_street_addr_building_name', 'primary_street_addr_unit_details',
    #             'primary_street_addr_street_no_name', 'primary_street_addr_po_box', 'primary_street_addr_city_suburb',
    #             'primary_street_addr_state', 'primary_street_addr_post_code', 'primary_street_addr_country',

    #             'RACGP_no',

    #             'gender', 'country_of_birth', 'city_of_birth', 'country_of_citizenship', 'citizenship_status',
    #             'aboriginal_origin', 'torres_strait_islander_origin', 'employment_status',
    #             'lang_identifier', 'english_proficiency', 'english_assistance', 'attending_other_school',
    #             'school_completed_level', 'survey_contact_status',

    #             'contact_name', 'relationship', 'contact_number','comments','contact_active','usi_exempt',

    #             'agent','USI'
    #         )
    #     )
    #     db.session.add(user)
    #     db.session.commit()
    #     send_password_create_email(user)
    #     flash('Congratulations, user has been created successfully. An email has been sent to create a password.')
    # return render_template('register.html', title='Register User', form=form)


@app.route('/user/admin/edit_user/<user_uuid>', methods=['GET', 'POST'])
@login_required
@role_required("Admin")
def edit_user(user_uuid):
    user = User.query.filter_by(user_uuid=user_uuid).first_or_404()
    #copying current user id for later query
    user_id = user.id

    radios = [
                'aboriginal_origin',
                'torres_strait_islander_origin',
                'english_assistance',
                'attending_other_school',
                'contact_active'
            ]
    form_user =  _update_fields_for_radio(user,radios)
    form = EditUserForm(formdata=request.form, obj=form_user)

    persistent_user = User.query.filter_by(user_uuid=user_uuid).first_or_404()
    if form.validate_on_submit():
        _save_user_changes(persistent_user, form)
        db.session.commit()
        flash('Your changes have been saved.')
        return redirect(url_for('users'))
    return render_template('register.html', title='Edit User', form=form)


@app.route('/user/admin/users', methods=['GET'])
@login_required
@role_required("Admin")
def users():
    page = request.args.get('page', 1, type=int)
    users = User.query.all()
    return render_template("users.html", title='Users', users=users)


@app.route('/user/admin/search_users', methods=['GET'])
@login_required
@role_required("Admin")
def search_users():
    search_term = request.args.get('keyword', None)
    if search_term:
        qs = User.query.filter(or_(User.email.ilike((f'%{search_term}%')),
                                   User.firstName.ilike((f'%{search_term}%')),
                                   User.lastName.ilike((f'%{search_term}%'))))
    else:
        qs = User.query.all()[:5]
    res = json.dumps([{'user_uuid': user.user_uuid, 'email': user.email,
                    'firstName':user.firstName,'lastName':user.lastName} for user in qs])
    return res


@app.route('/course/admin/search', methods=['GET'])
@login_required
@role_required("Admin","Instructor")
def search_courses():
    search_term = request.args.get('keyword', None)
    if search_term:
        qs = Course.query.filter(or_(Course.title.ilike((f'%{search_term}%')),
                                     Course.venue.ilike((f'%{search_term}%')),
                                     func.DATE(Course.date).ilike((f'%{search_term}%'))))
    else:
        qs = Course.query.all()[:5]
    res = json.dumps([{'id': course.id, 'title': course.title, 'description': course.descriptionPreview,
                       'date': course.date.strftime("%B %d,%Y"), 'start_time': course.startTime.strftime("%H:%M"),
                       'end_time': course.endTime.strftime("%H:%M"), 'cost': str(round(course.cost, 2)),
                       'venue': course.venue} for course in qs])
    return res


@app.route('/admin/course/email_to_attendees', methods=['POST'])
@login_required
@role_required("Admin","Instructor")
def email_to_attendees():
    data = dict(request.form)
    send_email_to_attendees.delay(data['courseId'], data['emailSubject'], data['emailMessage'])
    return {'data': 'Success'}
