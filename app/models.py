import json

import rq
import jwt
import redis
import uuid

from time import time
from datetime import datetime

from hashlib import md5
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash

from app import app, db, login
from app.search import add_to_index, remove_from_index, query_index

USER_CITIZENSHIP_STATUS_CHOICES = [
    ('Australian Citizen', 'Australian Citizen'),
    ('New Zealand Citizen', 'New Zealand Citizen'),
    ('Australian Permanent Resident', 'Australian Permanent Resident'),
    ('Student Visa', 'Student Visa'),
    ('Temporary Resident Visa', 'Temporary Resident Visa'),
    ("Visitor's Visa", "Visitor's Visa"),
    ('Business Visa', 'Business Visa'),
    ('Holiday Visa', 'Holiday Visa'),
    ('Other Visa', 'Other Visa'),
    ('Permanent Humanitarian Visa', 'Permanent Humanitarian Visa'),
    ('Overseas - No Visa or Citizenship', 'Overseas - No Visa or Citizenship')
]
USER_EMPLOYMENT_STATUS_CHOICES = [
    ('Full-time employee', 'Full-time employee'),
    ('Part-time employee', 'Part-time employee'),
    ('Self-employed - not employing others', 'Self-employed - not employing others'),
    ('Self-employed - employing others', 'Self-employed - employing others'),
    ('Employed - unpaid worker in a family business', 'Employed - unpaid worker in a family business'),
    ('Unemployed - Seeking full-time work', 'Unemployed - Seeking full-time work'),
    ('Unemployed - Seeking part-time work', 'Unemployed - Seeking part-time work'),
    ('Unemployed - Not Seeking employment', 'Unemployed - Not Seeking employment')
]
USER_ENGLISH_PROFICIENCY_CHOICES = [
    ('Very well', 'Very well'),
    ('Well', 'Well'),
    ('Not well', 'Not well'),
    ('Not at all', 'Not at all')
]
USER_SCHOOL_COMPLETED_LEVEL_CHOICES = [
    ('Year 12 or equivalent', 'Year 12 or equivalent'),
    ('Year 11 or equivalent', 'Year 11 or equivalent'),
    ('Year 10 or equivalent', 'Year 10 or equivalent'),
    ('Year 9 or equivalent', 'Year 9 or equivalent'),
    ('Year 8 or below', 'Year 8 or below'),
    ('Never attended school', 'Never attended school')
]
USER_STATE_CHOICES = [
    ('NSW', 'NSW'),
    ('VIC', 'VIC'),
    ('QLD', 'QLD'),
    ('SA', 'SA'),
    ('WA', 'WA'),
    ('TAS', 'TAS'),
    ('NT', 'NT'),
    ('ACT', 'ACT'),
    ('OTH (AUS)', 'OTH (AUS)'),
    ('OVS (Overseas)', 'OVS (Overseas)')
]
USER_SURVEY_CONTACT_STATUS_CHOICES = [
    ('A - Available for survey use', 'A - Available for survey use'),
    ('C - Correlational facility (address or enrolment)', 'C - Correlational facility (address or enrolment)'),
    ('D - Deceased student', 'D - Deceased student'),
    ('E - Excluded from survey use', 'E - Excluded from survey use'),
    ('I - Invalid address / Itinerant student (very low likelihood of response)',
     'I - Invalid address / Itinerant student (very low likelihood of response)'),
    ('M - Minor - under age of 15 (not to be surveyed)', 'M - Minor - under age of 15 (not to be surveyed)'),
    ('O - Overseas (address or enrolment)', 'O - Overseas (address or enrolment)')
]
USER_TITLE_CHOICES = [
    ('Mr', 'Mr'),
    ('Mrs', 'Mrs'),
    ('Miss', 'Miss'),
    ('Ms', 'Ms'),
    ('Other', 'Other')
]

USER_ROLE_CHOICES = [
    ('Client', 'Client'),
    ('Instructor', 'Instructor'),
    ('Admin', 'Admin')
]


@login.user_loader
def load_user(id):
    return User.query.get(int(id))


followers = db.Table(
    'followers',
    db.Column('follower_id', db.Integer, db.ForeignKey('user.id')),
    db.Column('followed_id', db.Integer, db.ForeignKey('user.id'))
)
quiz_template_course_quiz_assoc = db.Table(
    'quiz_template_course_quiz_assoc',
    db.Column('quiz_id', db.Integer, db.ForeignKey('quiz.id')),
    db.Column('quiz_template_id', db.Integer, db.ForeignKey('quiz_template.id'))
)

quiz_template_course_template_assoc = db.Table(
    'quiz_template_course_template_assoc',
    db.Column('quiz_template_id', db.Integer, db.ForeignKey('quiz_template.id')),
    db.Column('course_template_id', db.Integer, db.ForeignKey('template.id'))
)

class SearchableMixin(object):
    @classmethod
    def search(cls, expression, page, per_page):
        ids, total = query_index(cls.__tablename__, expression, page, per_page)
        if total == 0:
            return cls.query.filter_by(id=0), 0
        when = []
        for i in range(len(ids)):
            when.append((ids[i], i))
        return cls.query.filter(cls.id.in_(ids)).order_by(
            db.case(when, value=cls.id)), total

    @classmethod
    def before_commit(cls, session):
        session._changes = {
            'add': list(session.new),
            'update': list(session.dirty),
            'delete': list(session.deleted)
        }

    @classmethod
    def after_commit(cls, session):
        for obj in session._changes['add']:
            if isinstance(obj, SearchableMixin):
                add_to_index(obj.__tablename__, obj)
        for obj in session._changes['update']:
            if isinstance(obj, SearchableMixin):
                add_to_index(obj.__tablename__, obj)
        for obj in session._changes['delete']:
            if isinstance(obj, SearchableMixin):
                remove_from_index(obj.__tablename__, obj)
        session._changes = None

    @classmethod
    def reindex(cls):
        for obj in cls.query:
            add_to_index(cls.__tablename__, obj)


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_uuid = db.Column(db.String(50), default = str(uuid.uuid4()))
    firstName = db.Column(db.String(63))
    middleName = db.Column(db.String(63))
    lastName = db.Column(db.String(63))
    preferredName = db.Column(db.String(63))
    disabilities = db.Column(db.String(63))
    dob = db.Column(db.Date())

    # contact details
    mobile = db.Column(db.String(10))
    email = db.Column(db.String(120), index=True, unique=True)

    # address
    primary_postal_addr_building_name = db.Column(db.String(63))
    primary_postal_addr_unit_details = db.Column(db.String(63))
    primary_postal_addr_street_no_name = db.Column(db.String(63))
    primary_postal_addr_po_box = db.Column(db.String(63))
    primary_postal_addr_city_suburb = db.Column(db.String(63))
    primary_postal_addr_state = db.Column(db.String(7))
    primary_postal_addr_post_code = db.Column(db.String(7))
    primary_postal_addr_country = db.Column(db.String(31))

    primary_street_addr_building_name = db.Column(db.String(63))
    primary_street_addr_unit_details = db.Column(db.String(63))
    primary_street_addr_street_no_name = db.Column(db.String(63))
    primary_street_addr_po_box = db.Column(db.String(63))
    primary_street_addr_city_suburb = db.Column(db.String(63))
    primary_street_addr_state = db.Column(db.String(7))
    primary_street_addr_post_code = db.Column(db.String(7))
    primary_street_addr_country = db.Column(db.String(31))

    # custom fields
    RACGP_no = db.Column(db.String(15))

    # VET Related Details
    gender = db.Column(db.SmallInteger())
    country_of_birth = db.Column(db.String(31))
    city_of_birth = db.Column(db.String(63))
    country_of_citizenship = db.Column(db.String(31))
    citizenship_status = db.Column(db.String(31))
    aboriginal_origin = db.Column(db.Boolean(), default=False)
    torres_strait_islander_origin = db.Column(db.Boolean(), default=False)
    employment_status = db.Column(db.String(63))
    lang_identifier = db.Column(db.String(63))
    english_proficiency = db.Column(db.String(15))
    english_assistance = db.Column(db.Boolean(), default=False)
    attending_other_school = db.Column(db.Boolean(), default=False)
    school_completed_level = db.Column(db.String(31))
    survey_contact_status = db.Column(db.String(63))

    # emergency contact details
    contact_name = db.Column(db.String(63))
    relationship = db.Column(db.String(15))
    contact_number = db.Column(db.String(15))

    # additional information
    comments = db.Column(db.Text())
    contact_active = db.Column(db.Boolean(), default=False)

    password_hash = db.Column(db.String(128))
    urole = db.Column(db.String(80), default="Client")
    USI = db.Column(db.String(10), index=True)
    usi_exempt = db.Column(db.Boolean(), default=False)
    posts = db.relationship('Post', backref='author', lazy='dynamic')
    about_me = db.Column(db.String(140))
    last_seen = db.Column(db.DateTime, default=datetime.utcnow)
    followed = db.relationship(
        'User', secondary=followers,
        primaryjoin=(followers.c.follower_id == id),
        secondaryjoin=(followers.c.followed_id == id),
        backref=db.backref('followers', lazy='dynamic'), lazy='dynamic')
    messages_sent = db.relationship('Message',
                                    foreign_keys='Message.sender_id',
                                    backref='author', lazy='dynamic')
    messages_received = db.relationship('Message',
                                        foreign_keys='Message.recipient_id',
                                        backref='recipient', lazy='dynamic')
    last_message_read_time = db.Column(db.DateTime)
    notifications = db.relationship('Notification', backref='user',
                                    lazy='dynamic')
    tasks = db.relationship('Task', backref='user', lazy='dynamic')

    title = db.Column(db.String(7))

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return '<User {}>'.format(self.email)

    def avatar(self, size):
        digest = md5(self.email.lower().encode('utf-8')).hexdigest()
        return 'https://www.gravatar.com/avatar/{}?d=identicon&s={}'.format(
            digest, size)

    def follow(self, user):
        if not self.is_following(user):
            self.followed.append(user)

    def unfollow(self, user):
        if self.is_following(user):
            self.followed.remove(user)

    def is_following(self, user):
        return self.followed.filter(
            followers.c.followed_id == user.id).count() > 0

    def followed_posts(self):
        followed = Post.query.join(
            followers, (followers.c.followed_id == Post.user_id)).filter(
            followers.c.follower_id == self.id)
        own = Post.query.filter_by(user_id=self.id)
        return followed.union(own).order_by(Post.timestamp.desc())

    def get_reset_password_token(self, expires_in=600):
        return jwt.encode(
            {'reset_password': self.id, 'exp': time() + expires_in},
            app.config['SECRET_KEY'], algorithm='HS256').decode('utf-8')

    def new_messages(self):
        last_read_time = self.last_message_read_time or datetime(1900, 1, 1)
        return Message.query.filter_by(recipient=self).filter(
            Message.timestamp > last_read_time).count()

    def add_notification(self, name, data):
        self.notifications.filter_by(name=name).delete()
        n = Notification(name=name, payload_json=json.dumps(data), user=self)
        db.session.add(n)
        return n

    def launch_task(self, name, description, *args, **kwargs):
        rq_job = app.task_queue.enqueue('app.tasks.' + name, self.id,
                                        *args, **kwargs)
        task = Task(id=rq_job.get_id(), name=name, description=description,
                    user=self)
        db.session.add(task)
        return task

    def get_tasks_in_progress(self):
        return Task.query.filter_by(user=self, complete=False).all()

    def get_task_in_progress(self, name):
        return Task.query.filter_by(name=name, user=self,
                                    complete=False).first()

    def get_USI(self):
        return self.USI

    def get_urole(self):
        return self.urole

    @staticmethod
    def verify_reset_password_token(token):
        try:
            id = jwt.decode(token, app.config['SECRET_KEY'],
                            algorithms=['HS256'])['reset_password']
        except:
            return
        return User.query.get(id)


"""class Role(db.Model):
    #1=admin, 2=client, 3=instructor
    id = db.Column(db.Integer(), primary_key=True, default=2)
    name = db.Column(db.String(50), unique=True)

class UserRoles(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    user_id = db.Column(db.Integer(), db.ForeignKey('user.id', ondelete='CASCADE'))
    role_id = db.Column(db.Integer(), db.ForeignKey('role.id', ondelete='CASCADE'))"""


class Post(SearchableMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.String(140))
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    __searchable__ = ['body']

    def __repr__(self):
        return '<Post {}>'.format(self.body)


class Course(SearchableMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(140))
    course_code = db.Column(db.String(140))
    date = db.Column(db.DateTime, index=True)
    startTime = db.Column(db.Time)
    endTime = db.Column(db.Time)
    venue = db.Column(db.String(140))
    cost = db.Column(db.Numeric(50))
    maxAttendees = db.Column(db.Integer)
    description = db.Column(db.String(5000))
    descriptionPreview = db.Column(db.String(app.config['PREVIEW_LENGTH']))
    creator = db.Column(db.Integer, db.ForeignKey('user.id'))
    inhouse = db.Column(db.Boolean, default = False)
    inhouse_uuid = db.Column(db.String(50), default = str(uuid.uuid4()))
    instructor = db.Column(db.Integer(), db.ForeignKey('user.id', ondelete='CASCADE'))

    #close course from enrollment before start calculated prior to given days.
    #should be in days
    close_course_before_start =  db.Column(db.Integer,default=7)

    def __repr__(self):
        return '<Course {}>'.format(self.title)


class CourseAttendees(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    course_id = db.Column(db.Integer(), db.ForeignKey('course.id', ondelete='CASCADE'))
    user_id = db.Column(db.Integer(), db.ForeignKey('user.id', ondelete='CASCADE'))


class Template(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(140))
    venue = db.Column(db.String(140))
    cost = db.Column(db.Numeric(50))
    maxAttendees = db.Column(db.Integer)
    description = db.Column(db.String(5000))
    descriptionPreview = db.Column(db.String(app.config['PREVIEW_LENGTH']))
    creator = db.Column(db.Integer, db.ForeignKey('user.id'))

    quiz_templates = db.relationship('QuizTemplate',
                    secondary=quiz_template_course_template_assoc,
                    backref='course_template')

    # instructor
    # __searchable__ = ['body']

    def __repr__(self):
        return '<Course {}>'.format(self.title)

class QuizTemplate(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(128))
    image= db.Column(db.String(128))
    video_url = db.Column(db.String(128))
    questions = db.Column(db.JSON)

class Quiz(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    course_id = db.Column(db.Integer(), db.ForeignKey('course.id', ondelete='CASCADE'))
    quiz_templates = db.relationship('QuizTemplate',secondary=quiz_template_course_quiz_assoc, backref='quizes')
    course = db.relationship('Course')

class QuizResult(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    quiz_id = db.Column(db.Integer(), db.ForeignKey('quiz.id', ondelete='CASCADE'))
    user_id = db.Column(db.Integer(), db.ForeignKey('user.id', ondelete='CASCADE'))
    answers = db.Column(db.JSON)
    result = db.Column(db.String(140))

class Message(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    sender_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    recipient_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    body = db.Column(db.String(140))
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)

    def __repr__(self):
        return '<Message {}>'.format(self.body)


class Notification(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), index=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    timestamp = db.Column(db.Float, index=True, default=time)
    payload_json = db.Column(db.Text)

    def get_data(self):
        return json.loads(str(self.payload_json))


class Task(db.Model):
    id = db.Column(db.String(36), primary_key=True)
    name = db.Column(db.String(128), index=True)
    description = db.Column(db.String(128))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    complete = db.Column(db.Boolean, default=False)

    def get_rq_job(self):
        try:
            rq_job = rq.job.Job.fetch(self.id, connection=app.redis)
        except (redis.exceptions.RedisError, rq.exceptions.NoSuchJobError):
            return None
        return rq_job

    def get_progress(self):
        job = self.get_rq_job()
        return job.meta.get('progress', 0) if job is not None else 100


db.event.listen(db.session, 'before_commit', SearchableMixin.before_commit)
db.event.listen(db.session, 'after_commit', SearchableMixin.after_commit)
