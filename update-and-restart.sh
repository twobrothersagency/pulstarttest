#! /bin/bash
git pull
rm venv/bin/python3
ln -s /usr/bin/python3 venv/bin/python3
sudo supervisorctl stop pwa
cp ../.env .
flask db upgrade
sudo supervisorctl start pwa

#----------#
#For Mac Redis instance to run in foreground
#redis-server /usr/local/etc/redis.conf
