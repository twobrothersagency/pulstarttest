from celery.schedules import crontab

imports = ('app.tasks')
result_expires = 30
timezone = 'UTC'
enable_utc = True

accept_content = ['json', 'msgpack', 'yaml']
task_serializer = 'json'
result_serializer = 'json'

beat_schedule = {
    'reminder-email': {
        'task': 'app.tasks.reminder_email',
        # Every at midnight
        'schedule': crontab(minute=0, hour=0),
    },
    'course_reminder_email':{
        'task':'app.tasks.course_reminder_email',
        'schedule':crontab(minute=0,hour=0)
    },
    'final_reminder_email':{
        'task':'app.tasks.final_course_reminder_email',
        #runs every hour.
        'schedule':crontab(minute='*/60')
    },
    'quiz_reminder_email':{
        'task': 'app.tasks.send_course_quiz_remainder',
        'schedule': crontab(minute=0, hour=0),
    },
    'instructor_reminder_email':{
        'task': 'app.tasks.send_instructor_quiz_incomplete_notice',
        'schedule': crontab(minute=0, hour=0),
    },
}